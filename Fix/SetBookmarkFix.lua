--------------------------------------------------------------------------------
-- File:      Fix/SetBookmarkFix.lua
-- Date:      24/janv./2009 19:31
-- Author:    Valkea
-- File Rev:  2
-- Copyright: 2009
-- Desc: Update the TomeWindow.SetBookmark function so it work with bookmarks on both sides while the original function only work with bookmarks on left side of the Tome.
--------------------------------------------------------------------------------

local _hookSetBookmark
local _version = 2

-- Verify if already loaded
if SetBookmarkHook and SetBookmarkHook.version >= _version then return end
SetBookmarkHook = {}
SetBookmarkHook.version = _version
TomeWindow.INACTIVE_BOOKMARK_ANCHOR_X_RIGHT = 1190+86

local function SetBookMark(...)

	_hookSetBookmark(...)

	-- Reset all bookmarks
	for section = 1, TomeWindow.Sections.NUM_SECTIONS do
		WindowClearAnchors( TomeWindow.Sections[ section ].bookmarkWindow )
		local x = 0

		if (section <= 10) then	x = TomeWindow.INACTIVE_BOOKMARK_ANCHOR_X
		else x = TomeWindow.INACTIVE_BOOKMARK_ANCHOR_X_RIGHT end
	
		local y = TomeWindow.Sections[ section ].bookmarkAnchor.y
		WindowAddAnchor( TomeWindow.Sections[ section ].bookmarkWindow, "topleft", "TomeWindow", "topright", x, y )
	end
	
	-- Set the active bookmark
	local pageData = TomeWindow.Pages[ ... ]
	local section = pageData.sectionId
	
	WindowClearAnchors( TomeWindow.Sections[ section ].bookmarkWindow )
	
	local x = TomeWindow.Sections[ section ].bookmarkAnchor.x
	local y = TomeWindow.Sections[ section ].bookmarkAnchor.y
	  
	WindowAddAnchor( TomeWindow.Sections[ section ].bookmarkWindow, "topleft", "TomeWindow", "topright", x, y )
end

function SetBookmarkHook.Start()
	_hookSetBookmark = TomeWindow.SetBookmark
	TomeWindow.SetBookmark = SetBookMark
end