--------------------------------------------------------------------------------
-- File:      Conf/DK_Config.lua
-- Date:      26/janv./2009 7:35
-- Author:    Valkea
-- File Rev:  12
-- Copyright: 2009
--------------------------------------------------------------------------------

DK_Config = {}

local _parentFrame = "DKconfigWindow"
local _parentW = 490
local _parentH = 675
local _serverName = WStringToString(SystemData.Server.Name)
local _characterName = WStringToString(GameData.Player.name)
local _sounds = {}
local _soundsFilter = { ["2"] 	= "no sound",
			["103"] = "no sound",
			["104"] = "no sound",
			["105"] = "no sound",
			["106"] = "no sound",
			["107"] = "no sound",
			["108"] = "no sound",
			["109"] = "no sound",
			["111"] = "same as 307",
			["113"] = "same as 306",
			["114"] = "same as 307",
			["115"] = "same as 306",
			["116"] = "same as 307",
			["117"] = "same as 306",
			["118"] = "same as 307",
			["119"] = "same as 306",
			["120"] = "same as 307",
			["121"] = "no sound",
			["122"] = "no sound",
			["203"] = "no sound",
			["204"] = "no sound",
			["205"] = "no sound",
			["206"] = "no sound",
			["207"] = "no sound",
			["208"] = "no sound",
			["209"] = "no sound",
			["210"] = "no sound",
			["220"] = "no sound",
			["221"] = "no sound",
			["222"] = "no sound",
			["223"] = "no sound",
			["224"] = "no sound",
			["225"] = "no sound",
			["226"] = "no sound",
			["227"] = "no sound",
			["228"] = "no sound",
			["229"] = "no sound",
			["231"] = "no sound",
			["232"] = "no sound",
			["233"] = "no sound",
			["308"] = "same as 307",
			["998"] = "no sound",
			["1004"] = "no sound",
			["1112"] = "no sound",
			["1113"] = "no sound",
			}

-- Settings

local _settingJumper = {["DeathBlowsPrint"] 		= {1,1},
			["DeathBlowsAlert"] 		= {1,2},
			["DeathBlowsSound"] 		= {1,3},
			["DeathBlowsSoundCombo"]	= {1,4},

			["KillsPrint"] 			= {2,1},
			["KillsAlert"] 			= {2,2},
			["KillsSound"] 			= {2,3},
			["KillsSoundCombo"] 		= {2,4},

			["DeathsPrint"] 		= {3,1},
			["DeathsAlert"] 		= {3,2},
			["DeathsSound"] 		= {3,3},
			["DeathsSoundCombo"] 		= {3,4},

			["MiscHyperlink"] 		= {4,1},
			["MiscHyperlinkSC"]		= {4,2},
			["MiscMoth"]			= {4,3},
			["MiscHTScore"]			= {4,4}
			}

------------------------------------
-- Local functions -----------------
------------------------------------

local function print(text)
	TextLogAddEntry("Chat",10 , StringToWString(text))
end

local function printd(text)
	if(DammazKron.debug) then TextLogAddEntry("Chat",65 , StringToWString("DK DEBUG : "..text)) end
end

local function NumToBool ( v_num )
	if ( v_num == 0 ) then return false
	elseif ( v_num == 1 ) then return true
	else return nil end
end

local function BoolToNum ( v_bool )
	if ( v_bool == false ) then return 0
	elseif ( v_bool == true ) then return 1
	else return nil end
end

local function GetSettingIndex( settingName )
	return _settingJumper[settingName][1], _settingJumper[settingName][2]
end

local function UpdateSpecial( settingName, checked )
	if ( settingName == "MiscHyperlink" ) then
		ButtonSetDisabledFlag( _parentFrame..settingName.."SCButton", not checked )

	elseif ( settingName == "DeathBlowsSound" or settingName == "KillsSound" or settingName == "DeathsSound" ) then
		ComboBoxSetDisabledFlag( _parentFrame..settingName.."Combo", not checked )
		ButtonSetDisabledFlag( _parentFrame..settingName.."ComboPrev", not checked )
		ButtonSetDisabledFlag( _parentFrame..settingName.."ComboNext", not checked )

	elseif ( settingName == "MiscHTScore" ) then
		LabelSetText("DammazKronHTSLabel", L"")
		WindowSetShowing( "DammazKronHTS", checked )
	end
end

local function ComboBoxSetSound( v_windowName, v_table, v_default )
	for i, v in ipairs( v_table ) do
		ComboBoxAddMenuItem( v_windowName , StringToWString(v[3]) )
		if ( v[2] == v_default ) then ComboBoxSetSelectedMenuItem( v_windowName, i) end
	end
end

------------------------------------
-- Core functions ------------------
------------------------------------

local function Conf_Init()

	LabelSetText( _parentFrame.."TitleBarText", 		DammazKron.GetLocalString( "DKconfig_Title" ) )

	LabelSetText( _parentFrame.."DeathBlowsTitle", 		DammazKron.GetLocalString( "DKconfig_Title_DeathBlows" ) )
	LabelSetText( _parentFrame.."DeathBlowsPrintLabel", DammazKron.GetLocalString( "DKconfig_Print" ) )
	LabelSetText( _parentFrame.."DeathBlowsAlertLabel", DammazKron.GetLocalString( "DKconfig_Alert" ) )
	LabelSetText( _parentFrame.."DeathBlowsSoundLabel", DammazKron.GetLocalString( "DKconfig_Sound" ) )

	LabelSetText( _parentFrame.."KillsTitle", 			DammazKron.GetLocalString( "DKconfig_Title_Assists" ) )
	LabelSetText( _parentFrame.."KillsPrintLabel", 		DammazKron.GetLocalString( "DKconfig_Print" ) )
	LabelSetText( _parentFrame.."KillsAlertLabel", 		DammazKron.GetLocalString( "DKconfig_Alert" ) )
	LabelSetText( _parentFrame.."KillsSoundLabel", 		DammazKron.GetLocalString( "DKconfig_Sound" ) )

	LabelSetText( _parentFrame.."DeathsTitle", 			DammazKron.GetLocalString( "DKconfig_Title_Deaths" ) )
	LabelSetText( _parentFrame.."DeathsPrintLabel",		DammazKron.GetLocalString( "DKconfig_Print" ) )
	LabelSetText( _parentFrame.."DeathsAlertLabel",		DammazKron.GetLocalString( "DKconfig_Alert" ) )
	LabelSetText( _parentFrame.."DeathsSoundLabel", 	DammazKron.GetLocalString( "DKconfig_Sound" ) )

	LabelSetText( _parentFrame.."MiscTitle", 			DammazKron.GetLocalString( "DKconfig_Title_Misc" ) )
	LabelSetText( _parentFrame.."MiscHyperlinkLabel",	DammazKron.GetLocalString( "DKconfig_Hyperlink" ) )
	LabelSetText( _parentFrame.."MiscHyperlinkSCLabel",	DammazKron.GetLocalString( "DKconfig_HyperlinkSC" ) )
	LabelSetText( _parentFrame.."MiscMothLabel",		DammazKron.GetLocalString( "DKconfig_Moth" ) )
	LabelSetText( _parentFrame.."MiscHTScoreLabel",		DammazKron.GetLocalString( "DKconfig_HTScore" ) )

	--WindowSetDimensions( _parentFrame.."MiscHyperlink", LabelGetTextDimensions( _parentFrame.."MiscHyperlinkLabel" ) )
	WindowSetDimensions( _parentFrame , _parentW, _parentH)

	-- Sounds
	local i = #_sounds
	for k,v in DammazKron.pairsByKeys(GameData.Sound) do
		if ( not _soundsFilter[tostring(v)] ) then i = i + 1 _sounds[i] = {k, v, string.lower( string.gsub(k, "_", " ") ) } end
	end
end

----

function DK_Config.ToggleMenu()

	if not DoesWindowExist( _parentFrame ) then
		CreateWindow( _parentFrame , false)
		WindowSetShowing( _parentFrame , false)
		Conf_Init()
	end

	WindowSetShowing( _parentFrame , not WindowGetShowing( _parentFrame ))

	if ( WindowGetShowing( _parentFrame, true ) ) then

		ButtonSetPressedFlag( _parentFrame.."DeathBlowsPrintButton",	NumToBool( DK_Config.GetSetting( "DeathBlowsPrint" ) ) )
		ButtonSetPressedFlag( _parentFrame.."DeathBlowsAlertButton", 	NumToBool( DK_Config.GetSetting( "DeathBlowsAlert" ) ) )
		ButtonSetPressedFlag( _parentFrame.."DeathBlowsSoundButton", 	NumToBool( DK_Config.GetSetting( "DeathBlowsSound" ) ) )
		ComboBoxSetSound( _parentFrame.."DeathBlowsSoundCombo", _sounds, DK_Config.GetSetting( "DeathBlowsSoundCombo" ) )
	
		ButtonSetPressedFlag( _parentFrame.."KillsPrintButton", 	NumToBool( DK_Config.GetSetting( "KillsPrint" ) ) )
		ButtonSetPressedFlag( _parentFrame.."KillsAlertButton", 	NumToBool( DK_Config.GetSetting( "KillsAlert" ) ) )
		ButtonSetPressedFlag( _parentFrame.."KillsSoundButton", 	NumToBool( DK_Config.GetSetting( "KillsSound" ) ) )
		ComboBoxSetSound( _parentFrame.."KillsSoundCombo", _sounds, DK_Config.GetSetting( "KillsSoundCombo" ) )
	
		ButtonSetPressedFlag( _parentFrame.."DeathsPrintButton", 	NumToBool( DK_Config.GetSetting( "DeathsPrint" ) ) )
		ButtonSetPressedFlag( _parentFrame.."DeathsAlertButton", 	NumToBool( DK_Config.GetSetting( "DeathsAlert" ) ) )
		ButtonSetPressedFlag( _parentFrame.."DeathsSoundButton", 	NumToBool( DK_Config.GetSetting( "DeathsSound" ) ) )
		ComboBoxSetSound( _parentFrame.."DeathsSoundCombo", _sounds, DK_Config.GetSetting( "DeathsSoundCombo" ) )
	
		ButtonSetPressedFlag( _parentFrame.."MiscHyperlinkButton", 	NumToBool( DK_Config.GetSetting( "MiscHyperlink" ) ) )
		ButtonSetPressedFlag( _parentFrame.."MiscHyperlinkSCButton", 	NumToBool( DK_Config.GetSetting( "MiscHyperlinkSC" ) ) )
		ButtonSetPressedFlag( _parentFrame.."MiscMothButton", 		NumToBool( DK_Config.GetSetting( "MiscMoth" ) ) )
		ButtonSetPressedFlag( _parentFrame.."MiscHTScoreButton", 	NumToBool( DK_Config.GetSetting( "MiscHTScore" ) ) )

		ButtonSetDisabledFlag( _parentFrame.."MiscMothButton", not DammazKronPLUG.modIsEnabled( "Moth" ) )
	
		UpdateSpecial( "DeathBlowsSound", NumToBool( DK_Config.GetSetting( "DeathBlowsSound" ) ) )
		UpdateSpecial( "KillsSound", NumToBool( DK_Config.GetSetting( "KillsSound" ) ) )
		UpdateSpecial( "DeathsSound", NumToBool( DK_Config.GetSetting( "DeathsSound" ) ) )
		UpdateSpecial( "MiscHyperlink", NumToBool( DK_Config.GetSetting( "MiscHyperlink" ) ) )
	end
end

------------------------------------
-- DoIt functions ------------------
------------------------------------


function DK_Config.OnUpdateSoundCombo( v_type, v_activeWindow )
	local activeWindow
	if ( v_type == "PrevNext" ) then activeWindow = v_activeWindow else activeWindow = SystemData.ActiveWindow.name end
	local comboIndex = ComboBoxGetSelectedMenuItem( activeWindow )

	DK_Config.ToggleSetting( string.gsub(activeWindow, _parentFrame, ""), _sounds[comboIndex][2] )
	PlaySound(_sounds[comboIndex][2])
end

----

function DK_Config.OnPrevNextCombo()
	local activeWindow = SystemData.ActiveWindow.name
	local settingName = string.sub(activeWindow, -4)
	local comboName = string.sub(activeWindow, 1, -5)
	local index = ComboBoxGetSelectedMenuItem(comboName)

	if ( settingName == "Prev" and index > 1) then index = index - 1
	elseif ( settingName == "Next" and index < #_sounds) then index = index + 1 end

	ComboBoxSetSelectedMenuItem( comboName, index )
	DK_Config.OnUpdateSoundCombo( "PrevNext", comboName )
end

----

function DK_Config.OnUpdateSettingButton()
	local activeWindow = SystemData.ActiveWindow.name	
	local settingName = string.gsub(activeWindow, _parentFrame, "")
	local buttonName = activeWindow.."Button"

	if( ButtonGetDisabledFlag( buttonName ) ) then return end

	local checked = not ButtonGetPressedFlag(buttonName)
	ButtonSetPressedFlag( buttonName, checked )

	DK_Config.ToggleSetting( settingName, BoolToNum( checked ) )

	UpdateSpecial( settingName, checked )
end

----

function DK_Config.ToggleSetting( settingName, forceValue )
	local IndexA, IndexB = GetSettingIndex( settingName )
	local setting = DammazKron.data[_serverName][_characterName].Settings[IndexA][IndexB]

	if ( not forceValue and ( setting == 0 or setting == nil ) ) then setting = 1 else setting = 0 end
	if ( forceValue ) then setting = forceValue end

	DammazKron.data[_serverName][_characterName].Settings[IndexA][IndexB] = setting
end

----

function DK_Config.GetSetting( settingName )
	local IndexA, IndexB, returnValue
	
	if ( type(settingName) == "table" ) then IndexA, IndexB = settingName[1], settingName[2]
	else IndexA, IndexB = GetSettingIndex( settingName ) end

	returnValue = DammazKron.data[_serverName][_characterName].Settings[IndexA][IndexB]
	if ( returnValue ~= nil ) then return returnValue else return 0 end
end