--------------------------------------------------------------------------------
-- File:      Core/DK_Core.lua
-- Date:      18:54 26/04/2009
-- Author:    Valkea
-- File Rev:  54
-- Copyright: 2009
--------------------------------------------------------------------------------

------------------------------------
-- Global variables ----------------
------------------------------------

DammazKron = {}
DammazKron.ready = false
DammazKron.active = false
DammazKron.debug = false
DammazKron.sessionStats = {0,0,0,0,0,0,0,0}
DammazKron.lastGrudge = {"", 0, 0}
DammazKron.version = 0.630
DammazKron.errors = {}

------------------------------------
-- Local variables -----------------
------------------------------------

local _count = {0,0,0}
local _lastUpdate = {0,0,0}
local _lastParsed = {"",""}
local _isDead = false

local _hookShutdownPlayInterface
local _hookOnHyperLinkLButtonUp
local _hookOnHyperLinkRButtonUp

local _defaultSettings = { { 1, 1, 1, 219 }, { 0, 0, 0, 214 }, { 1, 1, 1, 215 }, { 1, 1, 1, 1 } }

local _serverName, _characterName, _characterShortName ,_characterFaction, _characterLevel, _characterXP

------------------------------------
-- Misc functions -----------------
------------------------------------

local function print( text )
	TextLogAddEntry("Chat",10 , text)
end

----

local function printd( text )
	if(DammazKron.debug) then TextLogAddEntry("Chat",65 , L"DK DEBUG : "..text) end
end

----

local function VerifyDB()
	for name, data in DammazKron.pairsByKeys(DammazKron.data[_serverName][_characterName]) do
		for Index = 1, 9 do
			if ( data[Index] == nil ) then
				data[Index] = 0
			elseif ( type( data[Index] ) == "string" ) then
				data[Index] = tonumber(data[Index])
			end
		end
	end
	d("DammazKron.VerifyDB")
end

----

local function CareerFillThemAll()
	for name, data in DammazKron.pairsByKeys(DammazKron.data[_serverName][_characterName]) do
		local t = DammazKron.data[_serverName][_characterName][name]
		if( t and ( t[8] == 0 or t[9] == 0 ) ) then
			for tmpName, tmpData in pairs(DammazKronTNFO.TmpTargetInfo) do
				if(name == tmpName) then
					t[8] = DammazKronTNFO.TmpTargetInfo[name][1]
					t[9] = DammazKronTNFO.TmpTargetInfo[name][2]
					DammazKronTNFO.TmpTargetInfo[name] = nil
					DammazKronTNFO.DictionnarySize = DammazKronTNFO.DictionnarySize - 1
					break
				end
			end
		end
		if(DammazKronTNFO.DictionnarySize <= 0) then break end
	end
	d("DammazKron.CareerFillThemAll")
end

----

local function RecountTotals( isPrint )
	local RecountTable = {0,0,0,0,0,0}
	local RowCount = 0

	for name, data in DammazKron.pairsByKeys(DammazKron.data[_serverName][_characterName]) do
		for Index = 1, 6 do RecountTable[Index] = RecountTable[Index] + data[Index] end
		RowCount = RowCount + 1
	end

	for Index = 1, 6 do
		DammazKron.data[_serverName][_characterName]["GlobalStats"][Index] = RecountTable[Index]
	end

	RecountTable[7] = RowCount
	
	if ( isPrint ) then print( DammazKron.GetLocalString("printRecount", RecountTable) ) end
	d("DammazKron.RecountTotals")
end

----

local function ToggleDebug()

	if (DammazKron.debug) then
		printd(L"OFF")
		DammazKron.debug = false
	else
		DammazKron.debug = true
		print(L"DK Debug is ON -> It use ChatFilters \"Channel 7\", \"Channel 8\" and \"Channel 9\"")
		printd(L"ON")
	end

	DammazKron.data.debugmode  = DammazKron.debug
end

----

local function ReturnList( order )

	local career, lvl, score
	for name, data in DammazKron.pairsByKeys(DammazKron.data[_serverName][_characterName], DammazKron.OrderBy( DammazKron.TYPE_LIST_OVERALL, tonumber(order))) do
		if( data[8] ~= 0 ) then career = DammazKronTNFO.CareerName( data[8] ) else career = L"." end
		if( data[9] ~= 0 ) then lvl = data[9] else lvl = "." end
		score = ( data[1] + data[2] ) - ( data[5] + data[6] )
		if ( data[10] ~= nil ) then ws_targetName = data[10] else ws_targetName = StringToWString( name ) end
		local color = DammazKron.linkColor( score )
		local link  = CreateHyperLink( L"DK_PROFIL:"..ws_targetName, L"["..ws_targetName..L"]", {color.r,color.g,color.b}, {} )
		print( DammazKron.GetLocalString("printAll", {tostring(link), career, lvl, data[1], data[2], data[3], data[4], data[5], data[6], score}) )
	end
	d("DammazKron.ReturnList")
end

----

local function PrintRatio()
	if(DammazKron.debug) then
		local ratio = tonumber ( _count[2] / _count[1] )
		print(L"--DebugRatio->"..ratio..L"<--Fired events->".._count[1]..L"<--Parsed events->".._count[2]..L"<--Unsolved events->".._count[3]..L"<-")
	else
		print(L"Require to toggle \"/dk debug\" ON")
	end
end

----

local function DebugCount( indexName, isSubstraction, infos )
	if(DammazKron.debug) then 
		local index, number = 0, 1
	
		    if ( indexName == "fired" ) then index = 1
		elseif ( indexName == "parsed" ) then index = 2 if ( infos ) then printd(L"PARSED :"..infos[1]) end
		elseif ( indexName == "error" ) then index = 3 table.insert( DammazKron.errors, infos ) printd(L"ERROR :"..infos[1])
		end
	
		if ( isSubstraction ) then number = -1 end
	
		_count[index] = _count[index] + number
	end
end

------------------------------------
-- Update functions ----------------
------------------------------------

local function Upgrade()
	--[[ Nothing to upgrade with this version
	if ( not DammazKron.data.version or tonumber(DammazKron.data.version) < tonumber(DammazKron.version) ) then
		if ( not DammazKron.data.version or tonumber(DammazKron.data.version) < 0.601 ) then Update0601() end
	end
	--]]
	DammazKron.data.version = DammazKron.version
end

------------------------------------
-- Hook functions ------------------
------------------------------------

local function OnHyperLinkLButtonUp( linkData, flags, x, y )
	_hookOnHyperLinkLButtonUp( linkData, flags, x, y )
	local ws_targetName, linkCount = wstring.gsub( linkData, L"DK_PROFIL:", L"" )
	if( linkCount > 0  ) then
		TomeWindow.DK_OnSelectProfil( ws_targetName )
		WindowSetShowing( "TomeWindow", true )
		return
	end
end

----

local function OnHyperLinkRButtonUp( linkData, flags, x, y )
	_hookOnHyperLinkRButtonUp( linkData, flags, x, y )
	local ws_targetName, linkCount = wstring.gsub( linkData, L"DK_PROFIL:", L"" )
	if( linkCount > 0  ) then
		DammazKronTTip.CreateWindow( ws_targetName )
		return
	end
end

------------------------------------
-- Register functions --------------
------------------------------------

local function AlertRegister( v_line1, v_line2, v_line3 )
	SystemData.AlertText.VecType = {11, 3, 12}
	SystemData.AlertText.VecText = { v_line1, v_line2, v_line3 }
	AlertTextWindow.AddAlert()
end

----

local function PrintRegister( ws_targetName, v_statIndex )

	local t = DammazKron.data[_serverName][_characterName]
	local StatIndex = math.ceil( v_statIndex / 2 )

	local printAllowed = 	DK_Config.GetSetting( {StatIndex, 1} )
	local alertAllowed = 	DK_Config.GetSetting( {StatIndex, 2} )
	local soundAllowed = 	DK_Config.GetSetting( {StatIndex, 3} )
	local linkAllowed = 	DK_Config.GetSetting( {4, 1} )
	local linkSC = 		DK_Config.GetSetting( {4, 2} )

	if ( soundAllowed ~= 0 ) then printd(L"Sound Alert-->"..towstring( DK_Config.GetSetting( {StatIndex, 4} ) )..L"<--") PlaySound( DK_Config.GetSetting( {StatIndex, 4} ) ) end

	if ( printAllowed == 0 and alertAllowed == 0 ) then return end

	local s_targetName = WStringToString( ws_targetName )
	local tStats = t[s_targetName]
	local careerLineSex = t[s_targetName][8]
	local careerName = DammazKronTNFO.CareerName(careerLineSex)
	local targetLevel = t[s_targetName][9]
	local printType

	local statDeathBlow = t[s_targetName][1]+t[s_targetName][2]
	local statDeaths = t[s_targetName][5]+t[s_targetName][6]
	local statScore = statDeathBlow - statDeaths
	local color = DammazKron.linkColor( statScore )
	
	    if ( StatIndex == 1 ) then printType = "DeathBlow"
	elseif ( StatIndex == 2 ) then printType = "Assist"
	elseif ( StatIndex == 3 ) then printType = "Death"
	end

	local tNFO
	if ( careerName == L"" and targetLevel == 0) then tNFO = ws_targetName else tNFO = DammazKron.GetLocalString( "alertLine2", {ws_targetName, careerName, targetLevel} ) end
	local LineString = DammazKron.GetLocalString( "alert"..printType, {tNFO} )
	local LineSplit = wstring.find (LineString, L"<br>")
	local Line1 = wstring.sub (LineString, 0, LineSplit-1)
	local Line2 = wstring.sub (LineString, LineSplit+4)
	local Line3 = DammazKron.GetLocalString( "alertLine3", { DammazKron.GetNumSymbol(statScore) } )

	if ( printAllowed ~= 0 ) then
		local Line0 = Line1..L" "..Line2..L" | "..Line3
		if ( linkAllowed ~= 0 ) then
			if ( linkSC == 0 ) then color = DefaultColor.ORANGE end
			local tLink = CreateHyperLink( L"DK_PROFIL:"..ws_targetName, L"["..ws_targetName..L"]", {color.r,color.g,color.b}, {} )
			Line0 = wstring.gsub(Line0, ws_targetName, tLink )
		end
		--print( WStringToString( Line0 ) )
		TextLogAddEntry("Chat",10 , Line0)
	end

	if ( alertAllowed ~= 0 ) then AlertRegister( Line1, Line2, Line3 ) end
end

----

local function RegisterPlayer( ws_targetName )
	local s_targetName = WStringToString( ws_targetName )
	if (DammazKron.data[_serverName][_characterName][s_targetName] == nil and s_targetName ~= "") then
		DammazKron.data[_serverName][_characterName][s_targetName] = {}
		for Index = 1, 9 do DammazKron.data[_serverName][_characterName][s_targetName][Index] = 0 end
		if ( ws_targetName ~= StringToWString( WStringToString( ws_targetName ) ) ) then DammazKron.data[_serverName][_characterName][s_targetName][10] = ws_targetName end --> Register only if UTF-8 Name != UTF-16 Name. 
	end
end

----

local function ResetPlayer( s_targetName )
	if (DammazKron.data[_serverName][_characterName][s_targetName] ~= nil and s_targetName ~= "") then
		printd(L"PROFIL RESET --->"..s_targetName.."<-")
		for Index = 1, 9 do DammazKron.data[_serverName][_characterName][s_targetName][Index] = 0 end
	end
end

----

local function UpdateTargetInfos( s_targetName )

	local t1 = DammazKron.data[_serverName][_characterName]
	local t2 = DammazKronTNFO.TmpTargetInfo

	if ( t2 and t2[s_targetName] and t1 and t1[s_targetName] ) then
		if ( t1[s_targetName][9] > t2[s_targetName][2] ) then ResetPlayer(s_targetName) end
		if ( t1[s_targetName][8] ~= t2[s_targetName][1] ) then t1[s_targetName][8] = t2[s_targetName][1] end
		if ( t1[s_targetName][9] ~= t2[s_targetName][2] ) then t1[s_targetName][9] = t2[s_targetName][2] end
	end
end

----

-- [1] = deathblows OpenRvR
-- [2] = deathblows Scenario
-- [3] = assists OpenRvR
-- [4] = assists Scenario
-- [5] = deaths OpenRvR
-- [6] = deaths Scenario
-- [7] = last Register Date
-- [8] = carrerLine + careerSex
-- [9] = level
-- [10] = UTF-16 Name

local function UpdateStat( StatIndex, ws_targetName, v_chatTimeSec )

	if ( ws_targetName ~= _characterShortName ) then

		local s_targetName = WStringToString( ws_targetName )
		
		if ( DammazKron.lastGrudge[1] == ws_targetName and math.abs( DammazKron.lastGrudge[2] - v_chatTimeSec ) <= 2 and DammazKron.lastGrudge[3] == StatIndex ) then printd(L"MultiUptade") return end
		if ( DammazKron.isInScenario() ) then StatIndex = StatIndex + 1 end
		local CurrentDate = DammazKron.GetCurrentDate()
		
		RegisterPlayer( ws_targetName )
		UpdateTargetInfos( s_targetName )
		
		DammazKron.data[_serverName][_characterName]["Timestamps"][StatIndex] = tonumber(CurrentDate)
		DammazKron.data[_serverName][_characterName]["GlobalStats"][StatIndex] = DammazKron.data[_serverName][_characterName]["GlobalStats"][StatIndex] + 1
		DammazKron.data[_serverName][_characterName][s_targetName][StatIndex] = DammazKron.data[_serverName][_characterName][s_targetName][StatIndex] + 1
		DammazKron.data[_serverName][_characterName][s_targetName][7] = tonumber(CurrentDate)
		
		DammazKron.sessionStats[StatIndex] = DammazKron.sessionStats[StatIndex] + 1
		PrintRegister( ws_targetName, StatIndex )
		
		DammazKronTTip.UpdateWindow( ws_targetName )
		
		DammazKron.lastGrudge = { ws_targetName, v_chatTimeSec, StatIndex }
		_lastUpdate[ math.ceil( StatIndex / 2 ) ] = v_chatTimeSec
	end
end

------------------------------------
-- Parse function ------------------
------------------------------------

local function isValidChannel( ChatChannel, v_verifType )

	local isValid = false
	if( ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_ORDER or ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION ) then
		isValid = true
		if ( v_verifType == "kill" or v_verifType == "killafterdeath" ) then
			    if ( _characterFaction == 1 and ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION ) then isValid = false
			elseif ( _characterFaction == 2 and ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_ORDER ) then isValid = false end
		elseif ( v_verifType == "death" )  then
			    if ( _characterFaction == 1 and ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_ORDER ) then isValid = false
			elseif ( _characterFaction == 2 and ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION ) then isValid = false end
		end
	end
	return isValid
end

----

local function PlayerKillPlayer( v_verifType )

	local LastIndex = TextLogGetNumEntries("Combat") - 1
	local LastTime, _, LastText = TextLogGetEntry("Combat", LastIndex)

	if ( LastTime == nil ) then printd(L"The Combat Log is empty.") return end
	if ( LastTime == _lastParsed[1] and LastText == _lastParsed[2] ) then return else _lastParsed[1] = LastTime _lastParsed[2] = LastText end
	if ( v_verifType == "death" and DammazKron.TstampToSecond( LastTime ) == _lastUpdate[3] ) then return end

	DebugCount( "fired" )

	local ChatTime, ChatChannel, ChatText
	local IsPvpEvent, KillerName, VictimName
	local LastTimeSec, ChatTimeSec, TimerSec = DammazKron.TstampToSecond(LastTime), nil, nil
	local IsValidChan
	local MaxCheckSec = 2

	while true do

		ChatTime, ChatChannel, ChatText = TextLogGetEntry("Combat", LastIndex)
		--ChatText = WStringToString(ChatText)
		ChatTimeSec = DammazKron.TstampToSecond(ChatTime)
		TimerSec = LastTimeSec - ChatTimeSec
		
		if( ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_ORDER or ChatChannel == SystemData.ChatLogFilters.RVR_KILLS_DESTRUCTION ) then
		
			IsValidChan = isValidChannel( ChatChannel, v_verifType )
				
			for k,v in pairs(DammazKron.GetLocalTable("searchAssist")) do
				IsPvpEvent ,_ ,VictimName ,KillerName = wstring.find(ChatText, v)
				--printd(L"-->"..towstring(IsPvpEvent)..L"<-->"..towstring(VictimName)..L"<-->"..towstring(KillerName)..L"<-->"..towstring(IsValidChan)..L"<-->"..towstring(ChatText)..L"<--")	
				if(IsPvpEvent ~= nil) then
				
					-- [ Suicides - we do not register it because suicide can be both pve or pvp event.
					if ( KillerName == VictimName and KillerName == _characterShortName ) then
						DebugCount( "parsed", false, {"Suicide"} ) break
						
					-- [ Deaths
					elseif ( IsValidChan and v_verifType == "death" and VictimName == _characterShortName ) then
						DebugCount( "parsed" ) UpdateStat(5, KillerName, ChatTimeSec) break
						
					-- [ Deathblows
					elseif ( IsValidChan and ( v_verifType == "kill" or v_verifType == "killafterdeath" ) and KillerName == _characterShortName ) then
						DebugCount( "parsed" ) UpdateStat(1, VictimName, ChatTimeSec) break
						
					-- [ Shared victories - we do not register it if this is a shared victory fired after death, or ally suicide or enemy assist.
					elseif ( ( v_verifType == "kill" or v_verifType == "killafterdeath" ) and KillerName ~= _characterShortName and VictimName ~= _characterShortName ) then
						DebugCount( "parsed" )
						if ( v_verifType == "killafterdeath" ) or ( IsValidChan == true  and VictimName == KillerName ) or ( IsValidChan == false and VictimName ~= KillerName ) then break end
						UpdateStat(3, VictimName, ChatTimeSec) break
						
					-- [ Events not parsed
					else
						DebugCount( "error", false, {"Not parsed", ChatTime, tostring(ChatText), ChatChannel, KillerName, tostring(IsValidChan)} )
						IsPvpEvent = nil
					end
				end
			end
		end
		
		if ( IsPvpEvent ~= nil ) then break end
		if ( v_verifType == "killafterdeath" ) then DebugCount( "parsed" ) break end
		if ( math.abs( LastTimeSec - ChatTimeSec ) > MaxCheckSec ) then	DebugCount( "error", false, {"Nothing found", ChatTime, tostring(ChatText), ChatChannel} ) break end
		LastIndex = LastIndex - 1
	end
end

------------------------------------
-- Commands and Slash functions ----
------------------------------------

local function PrintUsage()
	for i,v in pairs( DammazKron.GetLocalTable("usage") ) do print( v )end
	for i,v in pairs( DammazKron.GetLocalTable("usageDebug") ) do printd( v ) end
end

----

local function Command( input )

	if (not DammazKron.ready) then return end
	
	    if ( input == "help" or input == "" ) then PrintUsage()
	elseif ( input == "config" ) then DK_Config.ToggleMenu()
	elseif ( input == "version" ) then print( DammazKron.GetLocalString("version", {DammazKron.version}) )
	elseif ( input == "debug" ) then ToggleDebug()
	elseif ( input == "count" ) then RecountTotals( true )
	elseif ( input == "checkall" ) then CareerFillThemAll()
	elseif ( input == "verifydb" ) then VerifyDB()
	elseif ( input == "ratio" ) then PrintRatio()
	elseif ( input == "list" ) then ReturnList(false)
	elseif ( input == "test" ) then print(L"Test") print(L"->"..towstring(_characterName)..L"<->"..towstring(_characterShortName)..L"<->"..towstring(_characterFaction)..L"<->"..towstring(_characterLevel)..L"<->"..towstring(_characterXP)..L"<-")
	elseif ( string.sub(input,1,5) == "list-" ) then ReturnList( string.sub(input,6,-1) )
	else print( DammazKron.GetLocalString("promptError", {input}) )
	end
end

----

local function RegisterSlash()
	if (LibSlash == nil) then return end
	LibSlash.RegisterSlashCmd("dk", function(input) Command(input) end)
end

------------------------------------
-- EventHandler functions ----------
------------------------------------

local function RegisterEventHandlerDK()
	RegisterEventHandler( SystemData.Events.PLAYER_RVR_STATS_UPDATED,	"DammazKron.OnKillEvent" )
	RegisterEventHandler( SystemData.Events.PLAYER_DEATH, 				"DammazKron.OnDeathEvent" )
	RegisterEventHandler( SystemData.Events.PLAYER_DEATH_CLEARED, 		"DammazKron.OnDeathClearEvent" )
	RegisterEventHandler( SystemData.Events.TOGGLE_TOME_WINDOW, 		"TomeWindow.DK_ToggleTome" )
	RegisterEventHandler( SystemData.Events.LOADING_BEGIN, 				"DammazKron.OnDisable" )
	RegisterEventHandler( SystemData.Events.LOADING_END, 				"DammazKron.OnEnable" )
	RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, 		"DammazKron.OnReload" )
end

----

local function UnregisterEventHandlerDK()
	UnregisterEventHandler( SystemData.Events.PLAYER_RVR_STATS_UPDATED, "DammazKron.OnKillEvent" ) 	
	UnregisterEventHandler( SystemData.Events.PLAYER_DEATH, 			"DammazKron.OnDeathEvent" )
	UnregisterEventHandler( SystemData.Events.PLAYER_DEATH_CLEARED, 	"DammazKron.OnDeathClearEvent" )
	UnregisterEventHandler( SystemData.Events.TOGGLE_TOME_WINDOW, 		"TomeWindow.DK_ToggleTome" )
	UnregisterEventHandler( SystemData.Events.LOADING_BEGIN, 			"DammazKron.OnDisable" )
	UnregisterEventHandler( SystemData.Events.LOADING_END, 				"DammazKron.OnEnable" )
	UnregisterEventHandler( SystemData.Events.INTERFACE_RELOADED, 		"DammazKron.OnReload" )
end

------------------------------------
-- Misc. functions -----------------
------------------------------------

local function RegisterHTS()
	CreateWindow("DammazKronHTS", false)
	LayoutEditor.RegisterWindow( "DammazKronHTS", L"DammazKron HTS", L"Dammaz Kron Hostile Target Score", false, false, true, nil )
end

function DammazKron.AnchorHTS( type )

	if ( not DoesWindowExist( "DammazKronHTS" ) ) then RegisterHTS() end

	if ( WindowGetAnchorCount( "DammazKronHTS" ) > 0 ) then
		local _, _, relativeTo, _, _ = WindowGetAnchor ("DammazKronHTS", 1)
		if ( relativeTo == "Root" ) then return end
	end

	local DefWin = "TargetWindow"
	local PurWin = "PureTargetUnitFrameHostile"

	WindowClearAnchors( "DammazKronHTS" )

	    if ( type == "Pure" and DoesWindowExist( PurWin ) ) then WindowAddAnchor( "DammazKronHTS", "right", PurWin, "left", 0, 0 )
	elseif ( DoesWindowExist( DefWin ) ) then WindowAddAnchor( "DammazKronHTS", "right", DefWin, "left", 0, -2 )
	end
end

------------------------------------
-- Core functions ------------------
------------------------------------

local function InitializeLocal()
	_serverName = WStringToString( SystemData.Server.Name )
	_characterName = WStringToString( GameData.Player.name )
	_characterShortName = wstring.sub( GameData.Player.name,1,-3 )
	_characterFaction = GameData.Player.realm
	_characterLevel = GameData.Player.level
	_characterXP = GameData.Player.Experience.curXpEarned
end

----

local function InitializeData( serverName, characterName )

	if ( serverName and characterName ) then _serverName = serverName _characterName = characterName end
	
	-- [ Init DK data
	if (DammazKron.data == nil) then
		DammazKron.data = {}
		DammazKron.data.version = DammazKron.version
		print( DammazKron.GetLocalString("welcome") )
	else
		Upgrade()
	end

	if (DammazKron.data[_serverName] == nil) then DammazKron.data[_serverName] = {} end
	if (DammazKron.data[_serverName][_characterName] == nil or ( DammazKron.data[_serverName][_characterName] ~= nil and _characterLevel <= 1 and _characterXP == 0 ) ) then
		DammazKron.data[_serverName][_characterName] = {}
	end
	
	-- [ Init Global Stats
	if (DammazKron.data[_serverName][_characterName].GlobalStats == nil) then
		DammazKron.data[_serverName][_characterName].GlobalStats = {}
		for Index = 1, 6 do DammazKron.data[_serverName][_characterName].GlobalStats[Index] = 0 end
	end
	
	-- [ Init Dates
	if (DammazKron.data[_serverName][_characterName].Timestamps == nil) then
		DammazKron.data[_serverName][_characterName].Timestamps = {}
		for Index = 1, 6 do DammazKron.data[_serverName][_characterName].Timestamps[Index] = 0 end
	end

	-- [ Init Settings
	local t_settings = DammazKron.data[_serverName][_characterName].Settings
	if ( t_settings == nil )    then t_settings = {} end
	for i = 1, #_defaultSettings do
		if ( t_settings[i] == nil or  type(t_settings[i]) ~= "table" ) then t_settings[i] = {} end
		for j = 1, #_defaultSettings[i] do
			if ( t_settings[i][j] == nil ) then
				t_settings[i][j] = _defaultSettings[i][j]
				d("Init _defaultSettings["..i.."]["..j.."]=".._defaultSettings[i][j])
			end
		end
	end

	if ( t_settings[5] ~= nil ) then t_settings[5] = nil end

	DammazKron.data[_serverName][_characterName].Settings = t_settings

	if (DammazKron.data.debugmode ~= nil) then DammazKron.debug = DammazKron.data.debugmode else DammazKron.data.debugmode = DammazKron.debug end
end

----

function DammazKron.Start()
	RegisterEventHandler( SystemData.Events.ENTER_WORLD, "DammazKron.OnLoad" )
	RegisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "DammazKron.OnLoad" )
end

function DammazKron.OnLoad()

	InitializeLocal()
	InitializeData()
	RegisterSlash()
	VerifyDB()
	RegisterEventHandlerDK()
	DammazKron.AnchorHTS()

	TomeWindow.DK_Start()
	DammazKronTNFO.Start()
	DammazKronPLUG.Start()

	_hookShutdownPlayInterface = InterfaceCore.ShutdownPlayInterface
	InterfaceCore.ShutdownPlayInterface = DammazKron.Stop

	_hookOnHyperLinkLButtonUp = EA_ChatWindow.OnHyperLinkLButtonUp
	EA_ChatWindow.OnHyperLinkLButtonUp = OnHyperLinkLButtonUp

	_hookOnHyperLinkRButtonUp = EA_ChatWindow.OnHyperLinkRButtonUp
	EA_ChatWindow.OnHyperLinkRButtonUp = OnHyperLinkRButtonUp

	if( TextLogGetEnabled("Combat") == false ) then print(DammazKron.GetLocalString("errorCLog")) end
	
	DammazKron.ready = true
	DammazKron.active = true
	
	UnregisterEventHandler( SystemData.Events.ENTER_WORLD, "DammazKron.OnLoad" )
	UnregisterEventHandler( SystemData.Events.INTERFACE_RELOADED, "DammazKron.OnLoad" )
end

----

function DammazKron.Stop(...)

	if ( DammazKron.ready ) then
		CareerFillThemAll()
		RecountTotals()

		UnregisterEventHandlerDK()
		DammazKronTTip.UnregisterEventHandler()	
		DammazKronTNFO.Stop()

		DammazKron.ready = false
	end

	d("Dammaz Kron Close")

	return _hookShutdownPlayInterface(...)
end

------------------------------------
-- Handlers functions --------------
------------------------------------

function DammazKron.OnDeathEvent()
	if ( DammazKron.active ) then
		PlayerKillPlayer("death")
		RegisterEventHandler( TextLogGetUpdateEventId( "Combat" ), "DammazKron.OnCombatEvent" )
		_isDead = true
	end
end

----

function DammazKron.OnDeathClearEvent()
	if ( _isDead ) then
		UnregisterEventHandler( TextLogGetUpdateEventId( "Combat" ), "DammazKron.OnCombatEvent" )
		_isDead = false
	end
end

----

function DammazKron.OnKillEvent()
	if ( DammazKron.active ) then PlayerKillPlayer("kill") end
end

----

function DammazKron.OnCombatEvent()
	if ( DammazKron.active and _isDead ) then PlayerKillPlayer("killafterdeath") end
end

----

function DammazKron.OnDisable() 
	DammazKron.active = false
end

----

function DammazKron.OnEnable()
	DammazKron.active = true
	DammazKron.OnDeathClearEvent()
end

----

function DammazKron.OnReload() 
	DammazKron.active = true
end