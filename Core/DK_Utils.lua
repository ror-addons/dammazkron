--------------------------------------------------------------------------------
-- File:      Core/DK_Utils.lua
-- Date:      06:53 27/01/2009
-- Author:    Valkea
-- File Rev:  11
-- Copyright: 2009
--------------------------------------------------------------------------------

------------------------------------
-- Global variables ----------------
------------------------------------

DammazKron.ORDER_BY_DEATHBLOWS = 1
DammazKron.ORDER_BY_DEATHBLOWS_DESC = 2
DammazKron.ORDER_BY_KILLS = 3
DammazKron.ORDER_BY_KILLS_DESC = 4
DammazKron.ORDER_BY_DEATHS = 5
DammazKron.ORDER_BY_DEATHS_DESC = 6
DammazKron.ORDER_BY_SCORE = 7
DammazKron.ORDER_BY_SCORE_DESC = 8
DammazKron.ORDER_BY_CAREER = 9
DammazKron.ORDER_BY_CAREER_DESC = 10
DammazKron.ORDER_BY_LEVEL = 11
DammazKron.ORDER_BY_LEVEL_DESC = 12
DammazKron.ORDER_BY_NAME = 13
DammazKron.ORDER_BY_NAME_DESC = 14
DammazKron.ORDER_BY_DATE = 15
DammazKron.ORDER_BY_DATE_DESC = 16
DammazKron.ORDER_Array = { {1,2}, {3,4}, {5,6}, {7,8}, {9,10}, {11,12}, {13,14}, {15,16} } 

DammazKron.TYPE_LIST_OVERALL = 1
DammazKron.TYPE_LIST_OPENRVR = 2
DammazKron.TYPE_LIST_SCENARIO = 3

------------------------------------
-- Local variables -----------------
------------------------------------

--local _dayPerMonth 	= {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
--local _dayPerMonthBis = {31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}
local _dayToMonth 	= {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334}
local _dayToMonthBis 	= {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335}

local _serverName = WStringToString(SystemData.Server.Name)
local _characterName = WStringToString(GameData.Player.name)

local _LastTime, _LastTOKTime

------------------------------------
-- Misc. functions -----------------
------------------------------------

local function print(text)
	TextLogAddEntry("Chat",1 , StringToWString(text))
end

local function printd(text)
	if(DammazKron.debug) then TextLogAddEntry("Chat",65 , StringToWString("DK DEBUG : "..text)) end
end

----

local function TwoDigit(value)
	if (value < 10) then value = "0"..value end
	return value
end

----

local function IsBisextil(v_year)
	if ( v_year % 4 == 0 and ( v_year % 100 ~= 0 or v_year % 400 == 0 ) )
	then return true
	else return false end
end

------------------------------------
-- OrderBy functions ---------------
------------------------------------

function DammazKron.OrderBy(DK_Type, DK_Order)

	--printd("DK_Type->"..tostring(DK_Type).."<-DK_Order->"..tostring(DK_Order).."<-")

	local orderby
	local isSort01, isSort02, isSort03, isSort04

	    if ( DK_Order == DammazKron.ORDER_BY_DEATHBLOWS or DK_Order == DammazKron.ORDER_BY_KILLS or DK_Order == DammazKron.ORDER_BY_DEATHS ) then isSort01 = true
	elseif ( DK_Order == DammazKron.ORDER_BY_DEATHBLOWS_DESC or DK_Order == DammazKron.ORDER_BY_KILLS_DESC or DK_Order == DammazKron.ORDER_BY_DEATHS_DESC ) then isSort02 = true
	elseif ( DK_Order == DammazKron.ORDER_BY_SCORE ) then isSort03 = true
	elseif ( DK_Order == DammazKron.ORDER_BY_SCORE_DESC ) then isSort04 = true
	end

	if(DK_Order) then

		local orderIndex = DK_Order
		local tableref = DammazKron.data[_serverName][_characterName]

		    if ( DK_Order == DammazKron.ORDER_BY_CAREER ) then 		orderby = function (a, b) return tonumber( tableref[a][8] ) > tonumber( tableref[b][8] ) end
		elseif ( DK_Order == DammazKron.ORDER_BY_CAREER_DESC ) then	orderby = function (a, b) return tonumber( tableref[a][8] ) < tonumber( tableref[b][8] ) end
		elseif ( DK_Order == DammazKron.ORDER_BY_LEVEL ) then		orderby = function (a, b) return tonumber( tableref[a][9] ) > tonumber( tableref[b][9] ) end
		elseif ( DK_Order == DammazKron.ORDER_BY_LEVEL_DESC ) then	orderby = function (a, b) return tonumber( tableref[a][9] ) < tonumber( tableref[b][9] ) end
		elseif ( DK_Order == DammazKron.ORDER_BY_NAME ) then		orderby = function (a, b) return a > b end
		elseif ( DK_Order == DammazKron.ORDER_BY_NAME_DESC ) then	orderby = function (a, b) return a < b end
		elseif ( DK_Order == DammazKron.ORDER_BY_DATE ) then		orderby = function (a, b) return tonumber( tableref[a][7] ) > tonumber( tableref[b][7] ) end
		elseif ( DK_Order == DammazKron.ORDER_BY_DATE_DESC ) then	orderby = function (a, b) return tonumber( tableref[a][7] ) < tonumber( tableref[b][7] ) end
		
		elseif ( DK_Type == DammazKron.TYPE_LIST_OVERALL ) then
			    if ( isSort01 ) then 	orderby = function (a, b) return tonumber( tableref[a][orderIndex]+tableref[a][orderIndex+1] ) < tonumber( tableref[b][orderIndex]+tableref[b][orderIndex+1] ) end
			elseif ( isSort02 ) then 	orderby = function (a, b) return tonumber( tableref[a][orderIndex-1]+tableref[a][orderIndex] ) > tonumber( tableref[b][orderIndex-1]+tableref[b][orderIndex] ) end
			elseif ( isSort03 ) then 	orderby = function (a, b) return tonumber( (tableref[a][1]+tableref[a][2]) - (tableref[a][5]+tableref[a][6]) ) < tonumber( (tableref[b][1]+tableref[b][2]) - (tableref[b][5]+tableref[b][6]) ) end
			elseif ( isSort04 ) then 	orderby = function (a, b) return tonumber( (tableref[a][1]+tableref[a][2]) - (tableref[a][5]+tableref[a][6]) ) > tonumber( (tableref[b][1]+tableref[b][2]) - (tableref[b][5]+tableref[b][6]) ) end
			end

		elseif ( DK_Type == DammazKron.TYPE_LIST_OPENRVR ) then
			    if ( isSort01 ) then 	orderby = function (a, b) return tonumber( tableref[a][orderIndex] ) < tonumber( tableref[b][orderIndex] ) end
			elseif ( isSort02 ) then 	orderby = function (a, b) return tonumber( tableref[a][orderIndex-1] ) > tonumber( tableref[b][orderIndex-1] ) end
			elseif ( isSort03 ) then 	orderby = function (a, b) return tonumber( tableref[a][1] - tableref[a][5] ) < tonumber( tableref[b][1] - tableref[b][5] ) end
			elseif ( isSort04 ) then 	orderby = function (a, b) return tonumber( tableref[a][1] - tableref[a][5] ) > tonumber( tableref[b][1] - tableref[b][5] ) end
			end

		elseif ( DK_Type == DammazKron.TYPE_LIST_SCENARIO ) then
			    if ( isSort01 ) then 	orderby = function (a, b) return tonumber( tableref[a][orderIndex+1] ) < tonumber( tableref[b][orderIndex+1] ) end
			elseif ( isSort02 ) then 	orderby = function (a, b) return tonumber( tableref[a][orderIndex] ) > tonumber( tableref[b][orderIndex] ) end
			elseif ( isSort03 ) then 	orderby = function (a, b) return tonumber( tableref[a][2] - tableref[a][6] ) < tonumber( tableref[b][2] - tableref[b][6] ) end
			elseif ( isSort04 ) then 	orderby = function (a, b) return tonumber( tableref[a][2] - tableref[a][6] ) > tonumber( tableref[b][2] - tableref[b][6] ) end
			end
		end
	end

	return orderby
end

------------------------------------
-- Time functions ------------------
------------------------------------

function DammazKron.GetCurrentDate()

	local TodaysDate = GetTodaysDate()
	local LastTime, LastHour, LastMin, LastSec = "", "00", "00", "00"
	local todaysYear = TodaysDate.todaysYear
	local todaysMonth = TwoDigit(TodaysDate.todaysMonth)
	local todaysDay = TwoDigit(TodaysDate.todaysDay)
	
	if( TextLogGetNumEntries("Chat") > 2 ) then
		local LastIndex = TextLogGetNumEntries("Chat")-1
		LastTime, _, _ = TextLogGetEntry("Chat", LastIndex)
		LastTime = WStringToString(LastTime)
		LastTime = string.sub(LastTime,2,-2)
		LastTime = string.gsub(LastTime, ":", "")

		LastHour, LastMin, LastSec = string.sub(LastTime,1,2), string.sub(LastTime,3,4), string.sub(LastTime,5,6)

		local LastTOKTime = GameData.Tome.Statistics.playedTime
		local TimeOffset = 0

		if ( LastTime ~= _LastTime ) then _LastTime = LastTime _LastTOKTime = LastTOKTime
		elseif ( LastTOKTime ~= _LastTOKTime ) then TimeOffset = LastTOKTime - _LastTOKTime end

		LastMin = TwoDigit( LastMin + TimeOffset )
	end
	
	return todaysYear..todaysMonth..todaysDay..LastHour..LastMin..LastSec
end

----

function DammazKron.GetDate(v_dateIndex)
	local v_date = DammazKron.data[_serverName][_characterName]["Timestamps"][v_dateIndex]
	if(v_date ~= nil) then return v_date else return 0 end
end

----

function DammazKron.GetDateFormat(v_time)

	if(v_time ~= 0) then

		local year, month, day, hour, min, sec, ampm = string.sub(v_time,1,4), string.sub(v_time,5,6), string.sub(v_time,7,8), string.sub(v_time,9,10), string.sub(v_time,11,12), string.sub(v_time,13,14), ""
		
		if(DammazKron.GetLocalString("DK_TimeAMPM")) then
			if (tonumber(hour) > 12) then ampm = "pm" hour=hour-12 else ampm = "am"	end
		end
		
		return StringToWString( DammazKron.GetLocalString("DK_TimeFormat", {year, month, day, hour, min, sec, ampm}) )

	else return L"" end
end

----

function DammazKron.GetDateFormatSince(v_time)

	if(v_time ~= 0) then

		local c_time = DammazKron.GetCurrentDate()
		local year1, month1, day1, hour1, min1, sec1 = string.sub(v_time,1,4), string.sub(v_time,5,6), string.sub(v_time,7,8), string.sub(v_time,9,10), string.sub(v_time,11,12), string.sub(v_time,13,14)
		local year2, month2, day2, hour2, min2, sec2 = string.sub(c_time,1,4), string.sub(c_time,5,6), string.sub(c_time,7,8), string.sub(c_time,9,10), string.sub(c_time,11,12), string.sub(c_time,13,14)

		local daysToMonth1, daysToMonth2
		if ( IsBisextil( year1 ) ) then daysToMonth1 = _dayToMonthBis[ tonumber(month1) ] else daysToMonth1 = _dayToMonth[ tonumber(month1) ] end
		if ( IsBisextil( year2 ) ) then daysToMonth2 = _dayToMonthBis[ tonumber(month2) ] else daysToMonth2 = _dayToMonth[ tonumber(month2) ] end

		if ( year1 ~= year2 ) then
			if ( IsBisextil( year2 ) ) then daysToMonth1 =  daysToMonth1 - 365
			else daysToMonth1 =  daysToMonth1 - 366	end
		end

		local d1 = tonumber(sec1) + ( tonumber(min1) * 60 ) + ( tonumber(hour1) * 3600 ) + ( tonumber(day1) * 86400 ) + ( 86400 * daysToMonth1 )
		local d2 = tonumber(sec2) + ( tonumber(min2) * 60 ) + ( tonumber(hour2) * 3600 ) + ( tonumber(day2) * 86400 ) + ( 86400 * daysToMonth2 )
		local time = d2 - d1

		local timeString = L""
		local days, hours, min = 0,0,0
		if( ( time + 0.5 ) > 86400 ) then 
			days = math.floor( ( time + 0.5 ) / 86400 )
			time = time - days * 86400
			timeString = timeString..L" "..GetStringFormat( StringTables.Default.LABEL_X_D, { days } )
		end

		if( ( time + 0.5 ) > 3600 or days > 0 ) then 
			hours = math.floor( ( time + 0.5 ) / 3600 )
			time = time - hours * 3600
			timeString = timeString..L" "..GetStringFormat( StringTables.Default.LABEL_X_H, { TwoDigit(hours) } )
		end

		if( ( time + 0.5 ) > 60 or hours > 0 ) then
			min = math.floor( ( time + 0.5 ) / 60 )
			time = time - min * 60
			timeString = timeString..L" "..GetStringFormat( StringTables.Default.LABEL_X_M, { TwoDigit(min) } )
		end
		    
		--if( time > 0 ) then
		--	timeString = timeString..L" "..GetStringFormat( StringTables.Default.LABEL_X_S, { TwoDigit( math.floor( time + 0.5 ) ) } )
		--end

		return towstring( timeString )

	else return L"" end
end

----

function DammazKron.TstampToSecond(v_time)
	v_time = tostring(v_time)
	local hour, min, sec = string.sub(v_time,2,3), string.sub(v_time,5,6), string.sub(v_time,8,9)
	local totalsec = tonumber(sec) + ( 60 * tonumber(min) ) + ( 3600 * tonumber(hour) )
	return totalsec
end

------------------------------------
-- Miscellaneous functions ---------
------------------------------------

function DammazKron.GetNumSymbol( value )

	local returnValue
	if ( value > 0 ) then returnValue = "+"..value
	elseif ( value < 0 ) then returnValue = wstring.gsub( towstring( value ), L"-", L"-")	
	else returnValue = value end

	return towstring( returnValue )
end

function DammazKron.GetLastGrudge()
	local params = DammazKron.lastGrudge[1]

	if ( params == "" ) then
		for name, data in DammazKron.pairsByKeys( DammazKron.data[_serverName][_characterName], DammazKron.OrderBy( DammazKron.TYPE_LIST_OVERALL, DammazKron.ORDER_BY_DATE ) ) do
			if ( data[10] ~= nil ) then params = data[10] else params = StringToWString( name ) end
			break
		end
	end

	return params
end

----

function DammazKron.isInScenario()
	if( GameData.Player.isInScenario ) then
		if( GameData.Player.isInSiege == false ) then return true end
	end
end

----

function DammazKron.isValidProfilData( v_targetName )
	if( v_targetName ~= "GlobalStats" and v_targetName ~= "Timestamps" and v_targetName ~= "Settings" ) then return true end
end

----

function DammazKron.pairsByKeys ( t, forder, fverif, min, max )
	
	local a = {}

	if ( fverif ) then for n in pairs(t) do if ( DammazKron.isValidProfilData( n ) and fverif( n, t[n] ) ) then table.insert(a, n) end end
	else for n in pairs(t) do if ( DammazKron.isValidProfilData( n ) ) then table.insert(a, n) end end end

	table.sort(a, forder)

	if( not min ) then min = 0 end
	if( not max ) then max = #a end

	local i = min
	local iter = function ()
		i = i + 1
		--printd("Count1-i->"..i.."<-Min->"..tostring(min).."<-Max->"..tostring(max).."<-")	
		if a[i] == nil then return nil
		elseif ( i <= max ) then return a[i], t[a[i]], #a end
	end
	return iter
end

----

function DammazKron.linkColor( score )
	local color
	if ( score < 0 ) then color = DefaultColor.RED
	elseif ( score > 0 ) then color = DefaultColor.GREEN
	else color = DefaultColor.WHITE end
	return color
end