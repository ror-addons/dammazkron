--------------------------------------------------------------------------------
-- File:      Core/DK_Plugins.lua
-- Date:      26/janv./2009 7:36
-- Author:    Valkea
-- File Rev:  2
-- Copyright: 2009
--------------------------------------------------------------------------------

DammazKronPLUG = {}

local _serverName = WStringToString(SystemData.Server.Name)
local _characterName = WStringToString(GameData.Player.name)

------------------------------------
-- Moth functions ------------------
------------------------------------

local _hookMothUpdate, _cellNum, _rowIdx, _cellIdx

local function MothUpdateHook( Structure )

	_hookMothUpdate( Structure )

	if ( DK_Config.GetSetting( "MiscMoth" ) ~= 1 ) then return end

	if( TargetInfo:UnitType( "mouseovertarget" ) == 5 ) then

		local targetNameSex = WStringToString( TargetInfo:UnitName( "mouseovertarget" ) )
		local targetName = string.sub( targetNameSex ,1,-3)
		local targetTable = DammazKron.data[_serverName][_characterName][targetName]
	
		local thisCell = Structure[_rowIdx][_cellIdx]
		if ( thisCell and thisCell.type == "DKScore" ) then
			local cellText
			if ( targetTable == nil ) then cellText = 0 else cellText = ( targetTable[1]+targetTable[2] ) - ( targetTable[5]+targetTable[6] ) end
			Moth.SetCellText( "MothCell".._cellNum, L" "..DammazKron.GetLocalString("DK_LabelScore")..L" "..DammazKron.GetNumSymbol(cellText) , DammazKron.linkColor( cellText ), thisCell.font )
		end
	end
end

----

local function MothCellIdx( Structure )
	local cellNum = 0
	for rowIdx = 1, #Structure do
		for cellIdx = 1, #Structure[rowIdx] do
			cellNum = cellNum + 1
			local thisCell = Structure[rowIdx][cellIdx]
			local cellType = thisCell.type
			if ( cellType == "DKScore" ) then return cellNum, rowIdx, cellIdx end
		end
	end
end

----

local function MothInit()
	table.insert ( MothProfiles.Default.Structure[5], { type = "DKScore", font = "font_chat_text" } )
	Moth.CreateFramework( Moth.Profile.Structure )

	_cellNum, _rowIdx, _cellIdx = MothCellIdx( Moth.Profile.Structure )

	_hookMothUpdate = Moth.Populate
	Moth.Populate = MothUpdateHook
end

------------------------------------
-- Pure functions ------------------
------------------------------------

local _hookPureOnLoad

local function PureOnLoadHook( Structure )
	_hookPureOnLoad( Structure )
	DammazKron.AnchorHTS( "Pure" )
end

----

local function PureInit()
	_hookPureOnLoad = Pure.OnLoad
	Pure.OnLoad = PureOnLoadHook
end

------------------------------------
-- Core functions ------------------
------------------------------------

function DammazKronPLUG.Start()
	if ( DammazKronPLUG.modIsEnabled( "Moth" ) ) then MothInit() end
	if ( DammazKronPLUG.modIsEnabled( "Pure" ) ) then PureInit() end
end

----

function DammazKronPLUG.modIsEnabled( modName )
	local mods = ModulesGetData()
	for k,v in ipairs( mods ) do
		if v.name == modName and v.isEnabled then
			if not v.isLoaded then ModuleInitialize(v.name) end
			return true
		end
	end
	return false
end