--------------------------------------------------------------------------------
-- File:      Tome/DK_Tome.lua
-- Date:      02:40 23/02/2009
-- Author:    Valkea
-- File Rev:  13
-- Copyright: 2009
--------------------------------------------------------------------------------

------------------------------------
-- Local variables -----------------
------------------------------------

local _serverName = WStringToString(SystemData.Server.Name)
local _characterName = WStringToString(GameData.Player.name)
local _careerPictures = { 44,46,45,42,92,93,63,64,51,49,48,50,14,16,17,15,78,77,76,75,33,31,32,34 }
local _topArray = {}
local _paramsTOK = {}
local _paramsTOP = {0,0,0,0}
local _paramsNFO = {}
local _linePerPage = 19

------------------------------------
-- Local functions -----------------
------------------------------------

local function DK_SetTOCItem ( itemName, itemLabel, itemValue, itemIndex, itemButton )
	if (not itemButton) then ButtonSetDisabledFlag( itemName.."Text", true ) end
	TomeWindow.SetTOCItemText( itemName, itemIndex, L""..itemLabel, L""..itemValue )
	if ( tostring(itemLabel) == "" and tostring(itemValue) == "" ) then LabelSetText( itemName.."DottedLine", L"" ) end
end

----

local function print(text)
	TextLogAddEntry("Chat",0 , StringToWString(text))
end

local function printd(text)
	if(DammazKron.debug) then TextLogAddEntry("Chat",65 , StringToWString("DK DEBUG : "..text)) end
end

----

local function InitializeBookmark()

	----------------------------
	-- Initialize the Bookmark icon
	CreateWindowFromTemplate("DammazKronBookmark", "DammazKronBookmark", "TomeWindow")
	WindowSetOffsetFromParent("DammazKronBookmark",1190,176)
	--WindowSetDimensions("BOGWindowWnd", 86, 65)
	
	----------------------------
	-- Initialize the DammazKron contents
	CreateWindowFromTemplate("DammazKronTOK", "DammazKronTOK", "TomeWindow")
	CreateWindowFromTemplate("DammazKronTOP", "DammazKronTOP", "TomeWindow")
	CreateWindowFromTemplate("DammazKronNFO", "DammazKronNFO", "TomeWindow")
	--DynamicImageSetTextureOrientation (BOGWindowWnd, BOGWindowWndButton)

	----------------------------
	-- Update TomeWindow variables

	TomeWindow.Sections.NUM_SECTIONS = TomeWindow.Sections.NUM_SECTIONS + 1
	TomeWindow.Sections.SECTION_DAMMAZ_KRON = TomeWindow.Sections.NUM_SECTIONS

	TomeWindow.Pages.NUM_PAGES = TomeWindow.Pages.NUM_PAGES + 1
	TomeWindow.PAGE_DAMMAZ_KRON_TOK = TomeWindow.Pages.NUM_PAGES

	TomeWindow.Pages.NUM_PAGES = TomeWindow.Pages.NUM_PAGES + 1
	TomeWindow.PAGE_DAMMAZ_KRON_TOP = TomeWindow.Pages.NUM_PAGES

	TomeWindow.Pages.NUM_PAGES = TomeWindow.Pages.NUM_PAGES + 1
	TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO = TomeWindow.Pages.NUM_PAGES

	TomeWindow.Sections[ TomeWindow.Sections.SECTION_DAMMAZ_KRON ]
	= {
    		bookmarkWindow = "DammazKronBookmark",
    		bookmarkAnchor = { x=1250, y=176 },
   		sectionIcon = "MiniSection-Achievements"
  	}
end

----

local function InitializeTOK()

	local parentWindow = "DammazKronTOK"

	TomeWindow.Pages[ TomeWindow.PAGE_DAMMAZ_KRON_TOK ]
	      = TomeWindow.NewPageData( TomeWindow.Sections.SECTION_DAMMAZ_KRON,
					"DammazKronTOK", 
					TomeWindow.DK_Update_TOK,
					TomeWindow.OnDammazKronUpdateNavButtons,
					TomeWindow.OnDammazKronPreviousPage,
					TomeWindow.OnDammazKronNextPage,
					TomeWindow.OnDammazKronMouseOverPreviousPage,
					TomeWindow.OnDammazKronMouseOverNextPage )

	-- Title
	LabelSetText( parentWindow.."HeaderTitleLine1", DammazKron.GetLocalString("DK_Title_Line1") )
	LabelSetText( parentWindow.."HeaderTitleLine2", DammazKron.GetLocalString("DK_Title_Line2") )
 
	-- LifeTime & Session Stats
	LabelSetText( parentWindow.."StatsLifeTimeTitle", DammazKron.GetLocalString("DK_TitleLifeTime") )
	LabelSetText( parentWindow.."StatsSessionTitle", DammazKron.GetLocalString("DK_TitleSession") )

	-- Right Menu
	ButtonSetText( parentWindow.."Menu01", DammazKron.GetLocalString("DK_LeftMenu1") )
	ButtonSetText( parentWindow.."Menu02", DammazKron.GetLocalString("DK_LeftMenu2") )
	ButtonSetText( parentWindow.."Menu03", DammazKron.GetLocalString("DK_LeftMenu3") )
	ButtonSetText( parentWindow.."Menu04", DammazKron.GetLocalString("DK_LeftMenu4") )
end

----

local function InitializeTOP()

	TomeWindow.Pages[ TomeWindow.PAGE_DAMMAZ_KRON_TOP ]
	      = TomeWindow.NewPageData( TomeWindow.Sections.SECTION_DAMMAZ_KRON,
					"DammazKronTOP", 
					TomeWindow.DK_Update_TOP,
					TomeWindow.OnDammazKronUpdateNavButtons,
					TomeWindow.OnDammazKronPreviousPage,
					TomeWindow.OnDammazKronNextPage,
					TomeWindow.OnDammazKronMouseOverPreviousPage,
					TomeWindow.OnDammazKronMouseOverNextPage )

	local parentWindow = "DammazKronTOPPageWindow"
	local anchorWindow = "DammazKronTOPPageWindowContentsChildListAnchor"

	for LoopIndex = 1, _linePerPage do
		local questWindowName = "DammazKronTOPLine"..LoopIndex
		CreateWindowFromTemplate( questWindowName, "DammazKronTOPLine", parentWindow )
		WindowAddAnchor( questWindowName, "bottomleft", anchorWindow, "topleft", 0, 1 )
		anchorWindow = questWindowName
		WindowSetId( questWindowName, LoopIndex )  
	end

	-- Header line
	ButtonSetText( "DammazKronTOPHeadersSort6", DammazKron.GetLocalString("DK_Header6") )
	ButtonSetText( "DammazKronTOPHeadersSort7", DammazKron.GetLocalString("DK_Header7") )
	ButtonSetText( "DammazKronTOPHeadersSort8", DammazKron.GetLocalString("DK_Header8") )
	TomeWindow.DK_SetSortArrow( true, false, 8 )

	PageWindowAddPageBreak( "DammazKronTOPPageWindow", "DammazKronTOPPageWindowContentsChild" )
end

----

local function InitializeNFO()

	TomeWindow.Pages[ TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO ]
	      = TomeWindow.NewPageData( TomeWindow.Sections.SECTION_DAMMAZ_KRON,
					"DammazKronNFO", 
					TomeWindow.DK_Update_NFO,
					TomeWindow.OnDammazKronUpdateNavButtons,
					TomeWindow.OnDammazKronPreviousPage,
					TomeWindow.OnDammazKronNextPage,
					TomeWindow.OnDammazKronMouseOverPreviousPage,
					TomeWindow.OnDammazKronMouseOverNextPage )

	-- LifeTime & Session Stats

	local parent = "DammazKronNFOPageWindowR" 

	LabelSetText( parent.."LifeTimeTitle", DammazKron.GetLocalString("DK_NFO_LifeTimeTitle") )
	LabelSetText( parent.."OpenTitle", DammazKron.GetLocalString("DK_NFO_OpenTitle") )
	LabelSetText( parent.."ScenarioTitle", DammazKron.GetLocalString("DK_NFO_ScenarioTitle") )
	ButtonSetText( "DammazKronNFOMiniProfil", DammazKron.GetLocalString("DK_NFO_MiniProfilTracker") )
	
	--[[ -- Edit box
	LabelSetText( parent.."SessionTitle", DammazKron.GetLocalString("DK_TitleSession") )
	TextEditBoxSetText( "DammazKronNFOText", DammazKron.GetLocalString("DK_TitleSession") )
	WindowAssignFocus( "DammazKronNFOText", true )
	WindowSetShowing( "DammazKronNFOText", true )
    	WindowAssignFocus( "DammazKronNFOText", true ) --]]
end

------------------------------------
-- Core functions ------------------
------------------------------------

function TomeWindow.DK_Start()

	printd("Dammaz Kron TOK loaded START")
	InitializeBookmark()
	InitializeTOK()
	InitializeTOP()
	InitializeNFO()
end

----

function TomeWindow.DK_Stop()

end

------------------------------------
-- Update functions ----------------
------------------------------------

function TomeWindow.DK_Update_TOK()

	local parentWindow = "DammazKronTOK"

	-- Set Header Titles
	TomeWindow.SetPageHeaderText( TomeWindow.PAGE_DAMMAZ_KRON_TOK, DammazKron.GetLocalString("DK_HeaderTitleLeft"), L"" )

	-- Set LifeTime Stats
	local tGlobals = DammazKron.data[_serverName][_characterName].GlobalStats
	DK_SetTOCItem( parentWindow.."StatsLifeTimeDeathBlows", DammazKron.GetLocalString("DK_LabelDeathBlows"), 	tGlobals[1]+tGlobals[2], 100 )
	DK_SetTOCItem( parentWindow.."StatsLifeTimeKills", 		DammazKron.GetLocalString("DK_LabelKills"), 		tGlobals[3]+tGlobals[4], 101 )
	DK_SetTOCItem( parentWindow.."StatsLifeTimeDeaths", 	DammazKron.GetLocalString("DK_LabelDeaths"), 		tGlobals[5]+tGlobals[6], 102 )
	DK_SetTOCItem( parentWindow.."StatsLifeTimeScore", 		DammazKron.GetLocalString("DK_LabelScore"), 		DammazKron.GetNumSymbol( (tGlobals[1]+tGlobals[2]) - (tGlobals[5]+tGlobals[6]) ), 102 )

	DK_SetTOCItem( parentWindow.."StatsLastDeathBlow", 		DammazKron.GetLocalString("DK_LastDeathBlow"), 		DammazKron.GetDateFormatSince( math.max ( tonumber(DammazKron.GetDate(1)), tonumber(DammazKron.GetDate(2)) ) ), 100 )
	DK_SetTOCItem( parentWindow.."StatsLastKill", 			DammazKron.GetLocalString("DK_LastKill"), 			DammazKron.GetDateFormatSince( math.max ( tonumber(DammazKron.GetDate(3)), tonumber(DammazKron.GetDate(4)) ) ), 101 )
	DK_SetTOCItem( parentWindow.."StatsLastDeath", 			DammazKron.GetLocalString("DK_LastDeath"), 			DammazKron.GetDateFormatSince( math.max ( tonumber(DammazKron.GetDate(5)), tonumber(DammazKron.GetDate(6)) ) ), 102 )

	-- Set Session Stats
	DK_SetTOCItem( parentWindow.."StatsSessionDeathBlows", 	DammazKron.GetLocalString("DK_LabelDeathBlows"),	DammazKron.sessionStats[1]+DammazKron.sessionStats[2], 100 )
	DK_SetTOCItem( parentWindow.."StatsSessionKills", 		DammazKron.GetLocalString("DK_LabelKills"), 		DammazKron.sessionStats[3]+DammazKron.sessionStats[4], 101 )
	DK_SetTOCItem( parentWindow.."StatsSessionDeaths", 		DammazKron.GetLocalString("DK_LabelDeaths"), 		DammazKron.sessionStats[5]+DammazKron.sessionStats[6], 102 )
	DK_SetTOCItem( parentWindow.."StatsSessionScore", 		DammazKron.GetLocalString("DK_LabelScore"), 		DammazKron.GetNumSymbol( (DammazKron.sessionStats[1]+DammazKron.sessionStats[2]) - (DammazKron.sessionStats[5]+DammazKron.sessionStats[6]) ), 102 )
end

----

local function DK_Verify_TOP( name, t )

	local type = _paramsTOP[1]
	local stats = { { t[1] + t[2], t[3] + t[4], t[5] + t[6] }, { t[1], t[3], t[5] }, { t[2], t[4], t[6] } }
	
	if ( stats[type][1] ~= 0 or stats[type][2] ~= 0 or stats[type][3] ~= 0 ) then return true
	else return false end
end

function TomeWindow.DK_Update_TOP( type, order, page )

	_paramsTOP = { type, order, page, 0 }

	local careerLine, careerIconID, careerLevel, tendanceIcon, statDeathBlows, statKills, statDeaths, statScore, stats

	-- Set Header Titles
	TomeWindow.SetPageHeaderText( TomeWindow.PAGE_DAMMAZ_KRON_TOP,
                                 DammazKron.GetLocalString("DK_HeaderTitleLeft"), 
                                 DammazKron.GetLocalString("DK_HeaderTitleRightTOP"..type) )	

	-- Clean lines
	TomeWindow.DK_Clean_TOP("DammazKronTOPLine")

	-- Fill lines content
	local LineIndex = 1
	local LineMin = ( _paramsTOP[3] - 1 ) * _linePerPage
	local LineMax = ( _paramsTOP[3] ) * _linePerPage
	local ws_targetName

	for name, t, size in DammazKron.pairsByKeys( DammazKron.data[_serverName][_characterName], DammazKron.OrderBy( type, order ), DK_Verify_TOP, LineMin, LineMax ) do

		if ( t[10] ~= nil ) then ws_targetName = t[10] else ws_targetName = StringToWString( name ) end
		_paramsTOP[4] = math.ceil( size / _linePerPage)

		careerIcon = L"<icon43>"
		careerLevel = "<icon43>"

		if( t[8] ~= 0 and t[9] ~= 0 ) then 
			careerLine = string.sub( t[8] ,1,-2)
			careerIconID = Icons.GetCareerIconIDFromCareerLine( tonumber(careerLine) )
			if ( careerIconID ~= nil  ) then
				careerIcon = L"<icon"..careerIconID..L">"
				careerLevel = tostring(t[9])
			else
				t[8], t[9] = 0, 0 -- Reset corrupted Data
			end
		end

		tendanceIcon = "<icon41>"

		stats = { { t[1] + t[2], t[3] + t[4], t[5] + t[6] }, { t[1], t[3], t[5] }, { t[2], t[4], t[6] } }
		statDeathBlows = stats[type][1]
		statKills = stats[type][2]
		statDeaths = stats[type][3]
		statScore = statDeathBlows - statDeaths

		local questWindowName = "DammazKronTOPLine"..LineIndex

		LabelSetText ( questWindowName.."Icon", careerIcon )
		DK_SetTOCItem( questWindowName.."Name", ws_targetName, L"", 100, true )
		LabelSetText ( questWindowName.."Level", StringToWString( careerLevel ) )
		LabelSetText ( questWindowName.."Tendance", StringToWString( tendanceIcon ) )
		DK_SetTOCItem( questWindowName.."DeathBlows", L" ", towstring(statDeathBlows), 100, false )
		DK_SetTOCItem( questWindowName.."Kills", L" ", towstring(statKills), 100, false )
		DK_SetTOCItem( questWindowName.."Deaths", L" ", towstring(statDeaths), 100, false )
		DK_SetTOCItem( questWindowName.."Score", L" ", DammazKron.GetNumSymbol( statScore ), 100, false )
		DK_SetTOCItem( questWindowName.."Date", L" ", towstring( DammazKron.GetDateFormatSince( tonumber(t[7]), false ) ), 100, false )
		
		_topArray[LineIndex] = StringToWString( name )
		LineIndex = LineIndex + 1
	end

	TomeWindow.currentState.params[1] = _paramsTOP[1]
	TomeWindow.currentState.params[2] = _paramsTOP[2]
	TomeWindow.currentState.params[3] = _paramsTOP[3]
end

----

function TomeWindow.DK_Update_NFO( ws_targetName )

	local s_targetName = WStringToString( ws_targetName )
	local t = DammazKron.data[_serverName][_characterName][s_targetName]

	if ( not t ) then return end
	_paramsNFO = { ws_targetName }
	if ( t[10] ~= nil ) then ws_targetName = t[10] end

	local parent1 = "DammazKronNFOPageWindowL"
	local parent2 = "DammazKronNFOPageWindowR"
	local careerLine, careerIconID, careerName, careerPicID, careerLevel, careerIcon

	-- Header Titles
	TomeWindow.SetPageHeaderText( TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO,
                                 DammazKron.GetLocalString("DK_HeaderTitleLeft"), 
                                 DammazKron.GetLocalString("DK_HeaderTitleRightNFO", {ws_targetName}) ) 

	-- Player Infos
	LabelSetText( parent1.."Name", DammazKron.GetLocalString("DK_NFO_Name", {ws_targetName} ) )

	if( t[8] ~= 0 and t[9] ~= 0 ) then
		careerLine = string.sub( t[8] ,1,-2)
		careerName = DammazKronTNFO.CareerName( t[8] )
		careerLevel = t[9]
		careerPicID = _careerPictures[ tonumber(careerLine) ]
		careerIconID = Icons.GetCareerIconIDFromCareerLine( tonumber( careerLine ) )
		if ( careerIconID ~= nil ) then careerIcon = "<icon"..careerIconID..">" else careerIcon = "" end
		LabelSetText( parent1.."Class", DammazKron.GetLocalString("DK_NFO_CareerLevel", {careerName, careerLevel}) )
	else
		careerPicID = 112
		careerIcon = "<icon43>"
		LabelSetText( parent1.."Class", DammazKron.GetLocalString("DK_NFO_NoInfos") )
	end

	if ( not TomeIsBestiarySpeciesUnlocked( careerPicID ) ) then for i = 1, 150 do if ( TomeIsBestiarySpeciesUnlocked( i ) ) then careerPicID = i break end end end

	LabelSetText( parent1.."Icone1", StringToWString( careerIcon ) )

	-- Class Image
	TomeSetBestiarySpeciesImage( careerPicID )

	-- Stats
	DK_SetTOCItem( parent2.."LifeTimeDeathBlows", 	DammazKron.GetLocalString("DK_LabelDeathBlows"), t[1]+t[2], 100 )
	DK_SetTOCItem( parent2.."LifeTimeKills", 	DammazKron.GetLocalString("DK_LabelKills"), t[3]+t[4], 101 )
	DK_SetTOCItem( parent2.."LifeTimeDeaths", 	DammazKron.GetLocalString("DK_LabelDeaths"), t[5]+t[6], 102 )
	DK_SetTOCItem( parent2.."LifeTimeScore", 	DammazKron.GetLocalString("DK_LabelScore"), DammazKron.GetNumSymbol( (t[1]+t[2])-(t[5]+t[6]) ), 103 )

	DK_SetTOCItem( parent2.."OpenDeathBlows", 	DammazKron.GetLocalString("DK_LabelDeathBlows"), t[1], 100 )
	DK_SetTOCItem( parent2.."OpenKills", 		DammazKron.GetLocalString("DK_LabelKills"), t[3], 101 )
	DK_SetTOCItem( parent2.."OpenDeaths", 		DammazKron.GetLocalString("DK_LabelDeaths"), t[5], 102 )
	DK_SetTOCItem( parent2.."OpenScore", 		DammazKron.GetLocalString("DK_LabelScore"), DammazKron.GetNumSymbol( t[1]-t[5] ), 103 )

	DK_SetTOCItem( parent2.."ScenarioDeathBlows", 	DammazKron.GetLocalString("DK_LabelDeathBlows"), t[2], 100 )
	DK_SetTOCItem( parent2.."ScenarioKills", 	DammazKron.GetLocalString("DK_LabelKills"), t[4], 101 )
	DK_SetTOCItem( parent2.."ScenarioDeaths", 	DammazKron.GetLocalString("DK_LabelDeaths"), t[6], 102 )
	DK_SetTOCItem( parent2.."ScenarioScore", 	DammazKron.GetLocalString("DK_LabelScore"), DammazKron.GetNumSymbol( t[2]-t[6] ), 103 )

	-- LastSeen Date
	DK_SetTOCItem( parent1.."LastSeen", DammazKron.GetLocalString("DK_LastSeen"), DammazKron.GetDateFormatSince( tonumber(t[7]) ), 100 )
end

------------------------------------
-- DoIt functions -----------------
------------------------------------

function TomeWindow.DK_OnMouseoverBookmark()
	Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, StringToWString("Dammaz Kron") )
	Tooltips.AnchorTooltip( Tooltips.ANCHOR_WINDOW_RIGHT )
end

----

function TomeWindow.DK_OnBookmark()
	TomeWindow.SetState( TomeWindow.PAGE_DAMMAZ_KRON_TOK , {} )
end

----

function TomeWindow.DK_ToggleTome()
	if( WindowGetShowing("TomeWindow")) then
		--DammazKron.CareerFillThemAll()
		TomeWindow.DK_OnShow()
	end
end

----

function TomeWindow.DK_PageTopList()
	local topID = WindowGetId( SystemData.ActiveWindow.name )
	TomeWindow.SetState( TomeWindow.PAGE_DAMMAZ_KRON_TOP , {topID, DammazKron.ORDER_BY_DATE, 1} )
end

----

function TomeWindow.DK_OnSelectProfil( ws_targetName )
	local params, profilID

	if ( ws_targetName == 0 ) then
		params = { _topArray[ WindowGetId( WindowGetParent( SystemData.ActiveWindow.name) ) ] }
	else
		params = { ws_targetName }
	end

	if ( TomeWindow.GetCurrentState() == TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO ) then
		TomeWindow.PlayFlipAnim( TomeWindow.FLIP_FORWARD_SINGLE )
		TomeWindow.DK_Update_NFO( params[1] )
	else
		TomeWindow.SetState( TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO , params )
	end
end

----

function TomeWindow.DK_OnSelectProfilMini( ws_targetName )
	local params 
	local currentState = TomeWindow.GetCurrentState()

	if ( currentState == TomeWindow.PAGE_DAMMAZ_KRON_TOP ) then
		params = _topArray[ WindowGetId( WindowGetParent( SystemData.ActiveWindow.name) ) ] 
	elseif ( currentState == TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO ) then
		params = _paramsNFO[1]
	end

	if ( ws_targetName ) then DammazKronTTip.CreateWindow( params ) end
end

----

function TomeWindow.DK_CallPageLastGrudge()
	local params = DammazKron.GetLastGrudge()
	if params ~= "" then TomeWindow.SetState( TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO , { params } ) end
end

----

function TomeWindow.DK_OnRollOverSortButton()
	local index = WindowGetId( SystemData.ActiveWindow.name ) 
	local text1 = DammazKron.GetLocalString( "DK_OrderBy" ,{ DammazKron.GetLocalString( "DK_Header"..index ) } )
	local text2 = DammazKron.GetLocalString( "DK_HeaderDesc"..index )
	local parentWindow = "DammazKronTOPHeadersSort"..index
	
	if ( text1 ~= nil and text1 ~= "" ) then Tooltips.CreateTextOnlyTooltip( SystemData.ActiveWindow.name, text1 ) end
	if ( text2 ~= nil and text2 ~= "" ) then Tooltips.SetTooltipText( 2, 1, text2 ) Tooltips.Finalize() end
	local tipWidth, tipHeight = WindowGetDimensions (Tooltips.curTooltipWindow)
	Tooltips.AnchorTooltipManual( "top", parentWindow, "top", 0, - ( tipHeight + 10 ) )
end

----

function TomeWindow.DK_OnClickSortButton()
	local sortIndex = WindowGetId( SystemData.ActiveWindow.name )

	if ( _paramsTOP[2] == DammazKron.ORDER_Array[sortIndex][2] ) then _paramsTOP[2] = DammazKron.ORDER_Array[sortIndex][1]
	else _paramsTOP[2] = DammazKron.ORDER_Array[sortIndex][2] end

	TomeWindow.currentState.params[1] = _paramsTOP[1]
	TomeWindow.currentState.params[2] = _paramsTOP[2]
	TomeWindow.currentState.params[3] = _paramsTOP[3]

	TomeWindow.DK_Update_TOP( _paramsTOP[1], _paramsTOP[2], _paramsTOP[3] )
	TomeWindow.DK_SetSortArrow( _paramsTOP[2] == DammazKron.ORDER_Array[sortIndex][1], _paramsTOP[2] == DammazKron.ORDER_Array[sortIndex][2], sortIndex )
end

----

function TomeWindow.DK_OnShow(param)
	    if (TomeWindow.GetCurrentState() == TomeWindow.PAGE_DAMMAZ_KRON_TOK) then TomeWindow.DK_Update_TOK()
	elseif (TomeWindow.GetCurrentState() == TomeWindow.PAGE_DAMMAZ_KRON_TOP) then TomeWindow.DK_Update_TOP( _paramsTOP[1], _paramsTOP[2], _paramsTOP[3] )
	elseif (TomeWindow.GetCurrentState() == TomeWindow.PAGE_DAMMAZ_KRON_TARGET_INFO) then TomeWindow.DK_Update_NFO( _paramsNFO[1] )
	end
end

------------------------------------
-- Utils functions -----------------
------------------------------------

function TomeWindow.DK_Clean_TOP(questWindowName)

	local cleanArray1 = { "Name", "DeathBlows", "Kills", "Deaths", "Score", "Date" }
	local cleanArray2 = { "Icon", "Level", "Tendance" }

	for Index = 1, _linePerPage do
		for n in pairs(cleanArray1) do DK_SetTOCItem( questWindowName..Index..cleanArray1[n], L"", L"", 100, true ) end
		for n in pairs(cleanArray2) do LabelSetText( questWindowName..Index..cleanArray2[n], L"" ) end
	end
end

----

function TomeWindow.DK_SetSortArrow( showUp, showDown, sortIndex )

	local parentWindow = "DammazKronTOPHeadersSort"..sortIndex
	local xOffset = 10
	local xAnchor = "topright"
	local targetArrow = ""

	WindowSetShowing( "DK_HeaderUpArrow", showDown )
    	WindowSetShowing( "DK_HeaderDownArrow", showUp )

	local textWidth = ButtonGetTextDimensions( parentWindow )
	if( textWidth > 0 ) then
	        xOffset = xOffset + ( textWidth / 2 )
		xAnchor = "top"
	end

	if ( showDown ) then targetArrow = "DK_HeaderUpArrow"
	elseif ( showUp ) then targetArrow = "DK_HeaderDownArrow" end

	WindowClearAnchors( targetArrow )
	WindowAddAnchor( targetArrow , xAnchor, parentWindow, "top", xOffset, 7 )

end

------------------------------------
-- TomeWindow functions ------------
------------------------------------

function TomeWindow.OnDammazKronUpdateNavButtons()
	if ( TomeWindow.GetCurrentState() ~= TomeWindow.PAGE_DAMMAZ_KRON_TOP ) then
		WindowSetShowing( "TomeWindowPreviousPageButton", false )
		WindowSetShowing( "TomeWindowNextPageButton", false ) 
		return
	else
		WindowSetShowing( "TomeWindowPreviousPageButton", _paramsTOP[3] > 1 )
		WindowSetShowing( "TomeWindowNextPageButton", _paramsTOP[3] + 1 <= _paramsTOP[4] ) 
	end 
end

----

function TomeWindow.OnDammazKronPreviousPage()
	if( _paramsTOP[3] - 1 < 1 ) then return false end
	_paramsTOP[3] = _paramsTOP[3] - 1
	TomeWindow.PlayFlipAnim( TomeWindow.FLIP_BACKWARD_SINGLE )
	TomeWindow.DK_Update_TOP( _paramsTOP[1], _paramsTOP[2], _paramsTOP[3] )
    	return true
end

----

function TomeWindow.OnDammazKronNextPage()
	if( _paramsTOP[3] + 1 > _paramsTOP[4] ) then return false end
	_paramsTOP[3] = _paramsTOP[3] + 1
	TomeWindow.PlayFlipAnim( TomeWindow.FLIP_FORWARD_SINGLE )
	TomeWindow.DK_Update_TOP( _paramsTOP[1], _paramsTOP[2], _paramsTOP[3] )
    	return true
end

----

function TomeWindow.OnDammazKronMouseOverPreviousPage()    
end

function TomeWindow.OnDammazKronMouseOverNextPage()    
end