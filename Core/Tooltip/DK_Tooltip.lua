--------------------------------------------------------------------------------
-- File:      Core/DK_Tooltip.lua
-- Date:      04:46 23/02/2009
-- Author:    Valkea
-- File Rev:  11
-- Copyright: 2009
--------------------------------------------------------------------------------

DammazKronTTip = {}

------------------------------------
-- Local variables -----------------
------------------------------------

local _serverName = WStringToString(SystemData.Server.Name)
local _characterName = WStringToString(GameData.Player.name)

local _windowName = "DK_MiniProfil_"
local _icon_buffer = 45
local _border_buffer = 15
local _MiniWindows = {}
local _registerON = false

------------------------------------
-- Local functions -----------------
------------------------------------

local function print(text)
	TextLogAddEntry("Chat",10 , StringToWString(text))
end

local function printd(text)
	if(DammazKron.debug) then TextLogAddEntry("Chat",65 , StringToWString("DK DEBUG : "..text)) end
end

------------------------------------
-- EventHandler functions ----------
------------------------------------

function DammazKronTTip.RegisterEventHandler()
	if ( _registerON == true ) then return else _registerON = true end
	RegisterEventHandler( SystemData.Events.TOME_STAT_PLAYED_TIME_UPDATED, "DammazKronTTip.UpdateAll" )
	printd("DammazKronTTip.RegisterEventHandler")
end

function DammazKronTTip.UnregisterEventHandler()
	if ( _registerON == false ) then return else _registerON = false end
	UnregisterEventHandler( SystemData.Events.TOME_STAT_PLAYED_TIME_UPDATED, "DammazKronTTip.UpdateAll" )
	printd("DammazKronTTip.UnregisterEventHandler")
end

------------------------------------
-- Core functions ------------------
------------------------------------

function DammazKronTTip.CreateWindow( ws_targetName )

	local s_targetName = WStringToString( ws_targetName )
	local t = DammazKron.data[_serverName][_characterName][s_targetName]
	if ( not t ) then return end

	local windowName = _windowName..s_targetName
	if ( not DoesWindowExist( windowName ) ) then
	
		CreateWindowFromTemplate( windowName, "DammazKron_ProfilLinkTemplate", "Root" )
		WindowAddAnchor( windowName, "center", "Root", "center", 0, 0 )
		
		table.insert( _MiniWindows, ws_targetName)
		if ( #_MiniWindows > 0 ) then DammazKronTTip.RegisterEventHandler() end
	end
	
	DammazKronTTip.UpdateWindow( ws_targetName )
	--DammazKronTTip.UpdateAll()
end

----

function DammazKronTTip.UpdateWindow( ws_targetName, isEventUpdate )
	local s_targetName = WStringToString( ws_targetName )
	local windowName = _windowName..s_targetName
	if ( not DoesWindowExist( windowName ) ) then return end 

	local t = DammazKron.data[_serverName][_characterName][s_targetName]
	if ( not t ) then return end

	DammazKronTTip.SetData( windowName.."Data" , ws_targetName, t )
	ForceProcessAllWindowAnchors()
	local x, y = WindowGetDimensions( windowName.."DataBackground" )
	WindowSetDimensions( windowName, x+15, y+15 )
	
	if ( not isEventUpdate ) then WindowSetShowing( windowName, true ) end
end

----

function DammazKronTTip.UpdateAll()
	for i,v in ipairs(_MiniWindows) do DammazKronTTip.UpdateWindow( v, true ) end
end

----

function DammazKronTTip.SetData( windowName, ws_targetName, profilData )

	if ( profilData[10] ~= nil ) then ws_targetName = profilData[10] end
	local careerLine = string.sub( profilData[8] ,1,-2)
	local careerName = DammazKronTNFO.CareerName( profilData[8] )
	local careerIconID = Icons.GetCareerIconIDFromCareerLine( tonumber( careerLine ) )
	local careerLevel = profilData[9]

	local statDeathBlow = profilData[1] + profilData[2]
	local statAssists = profilData[3] + profilData[4]
	local statDeaths = profilData[5] + profilData[6]
	local statScore = statDeathBlow - statDeaths
	local scoreColor = DammazKron.linkColor( statScore )

	local iconID, iconScale = 43, 1.65
	if( profilData[8] ~= 0 and profilData[9] ~= 0 ) then iconScale = 1.26 iconID = Icons.GetCareerIconIDFromCareerLine( tonumber(careerLine) ) end
	local texture, x, y = GetIconData( iconID )

	-- Profil
	DynamicImageSetTexture( windowName.."Icon", texture, x, y )
	DynamicImageSetTextureScale( windowName.."Icon", iconScale )

	LabelSetText( windowName.."Name", 		ws_targetName )
	LabelSetText( windowName.."Career", 		careerName )
	if ( careerLevel > 0 ) then
		LabelSetText( windowName.."Level", 	DammazKron.GetLocalString("DK_NFO_Level", {careerLevel}) )
	end

	-- Stats
	LabelSetText( windowName.."DeathblowTitle", DammazKron.GetLocalString("DK_LabelDeathBlows") )
	LabelSetText( windowName.."AssistTitle", 	DammazKron.GetLocalString("DK_LabelKills") )
	LabelSetText( windowName.."DeathTitle", 	DammazKron.GetLocalString("DK_LabelDeaths") )
	LabelSetText( windowName.."ScoreTitle", 	DammazKron.GetLocalString("DK_LabelScore") )

	LabelSetText( windowName.."DeathblowValue", towstring( statDeathBlow ) )
	LabelSetText( windowName.."AssistValue", 	towstring( statAssists ) )
	LabelSetText( windowName.."DeathValue", 	towstring( statDeaths ) )
	LabelSetText( windowName.."ScoreValue", 	DammazKron.GetNumSymbol(statScore) )
	LabelSetTextColor( windowName.."ScoreValue",	scoreColor.r, scoreColor.g, scoreColor.b )

	-- Date
	if ( tonumber( profilData[7] ) > 0 ) then
		LabelSetText ( windowName.."Date", 	DammazKron.GetLocalString( "DK_LastSeen" )..L"<br>"..DammazKron.GetDateFormatSince( tonumber( profilData[7] ) ) )
	end

	-- Resizing/reformating
	local minWidth = math.max (
		LabelGetTextDimensions( windowName.."Name" ) + _icon_buffer + 5,
		LabelGetTextDimensions( windowName.."Career" ),
		LabelGetTextDimensions( windowName.."Level" ),
		LabelGetTextDimensions( windowName.."DeathblowTitle" ) + LabelGetTextDimensions( windowName.."DeathblowValue" ) + 5,
		LabelGetTextDimensions( windowName.."AssistTitle" ) + LabelGetTextDimensions( windowName.."AssistValue" ) + 5,
		LabelGetTextDimensions( windowName.."DeathTitle" ) + LabelGetTextDimensions( windowName.."DeathValue" ) + 5,
		LabelGetTextDimensions( windowName.."ScoreTitle" ) + LabelGetTextDimensions( windowName.."ScoreValue" ) + 5,
		LabelGetTextDimensions( windowName.."Date" )
	)
	
	local x, y = WindowGetDimensions( windowName.."Background" )
	WindowSetDimensions( windowName.."Background", minWidth+30, y )
	WindowSetDimensions( windowName.."HeaderSeperator01", minWidth, 2 )
	WindowSetDimensions( windowName.."HeaderSeperator02", minWidth, 2 )
	WindowSetDimensions( windowName.."HeaderSeperator03", minWidth, 2 )
end

----

function DammazKronTTip.DestroyWindow()

	local windowName = WindowGetParent( SystemData.ActiveWindow.name)
	DestroyWindow( windowName )

	-- Remove from table
	for i,v in ipairs(_MiniWindows) do
		if ( _windowName..WStringToString( v ) == windowName ) then table.remove( _MiniWindows, i) break end
	end

	if ( #_MiniWindows < 1 ) then DammazKronTTip.UnregisterEventHandler() end
end