--------------------------------------------------------------------------------
-- File:      Core/DK_TargetInfo.lua
-- Date:      04:40 03/03/2009
-- Author:    Valkea
-- File Rev:  14
-- Copyright: 2009
--------------------------------------------------------------------------------

DammazKronTNFO = {}
DammazKronTNFO.TmpTargetInfo = {}
DammazKronTNFO.DictionnarySize = 0

local _serverName = WStringToString(SystemData.Server.Name)
local _characterName = WStringToString(GameData.Player.name)

local function printd(text)
	if(DammazKron.debug) then TextLogAddEntry("Chat",64 , StringToWString("DKNFO DEBUG : "..text)) end
end

------------------------------------
-- Core functions ------------------
------------------------------------

function DammazKronTNFO.Start()
	RegisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "DammazKronTNFO.PlayerTargetUpdate")
end

----

function DammazKronTNFO.Stop()
	UnregisterEventHandler(SystemData.Events.PLAYER_TARGET_UPDATED, "DammazKronTNFO.PlayerTargetUpdate")	
end

------------------------------------
-- DoIt functions ------------------
------------------------------------

function DammazKronTNFO.PlayerTargetUpdate( targetClassification, targetId, targetType )
	TargetInfo:UpdateFromClient()
	local targetNameSex = TargetInfo:UnitName( targetClassification )
	local s_targetName = WStringToString( wstring.sub( targetNameSex ,1,-3) )	

	if ( targetType == 5 and s_targetName ~= "" ) then

		if(DammazKronTNFO.TmpTargetInfo[s_targetName] == nil) then
			
			local careerLine = TargetInfo:UnitCareer( targetClassification )
			local targetLevel = TargetInfo:UnitLevel( targetClassification )
			
			if ( tonumber(careerLine) == 0 or tonumber(targetLevel) == 0 ) then return end
			local targetSex = wstring.sub( targetNameSex, -1 )
			if( targetSex == "F" ) then targetSex = 1 else targetSex = 0 end
			local careerLineSex = ( tonumber(careerLine) * 10 ) + tonumber(targetSex)
			
			DammazKronTNFO.TmpTargetInfo[s_targetName] = {}
			DammazKronTNFO.TmpTargetInfo[s_targetName][1] = careerLineSex
			DammazKronTNFO.TmpTargetInfo[s_targetName][2] = targetLevel
			DammazKronTNFO.DictionnarySize = DammazKronTNFO.DictionnarySize + 1
			
			printd("New TNFO -->"..s_targetName.."<-->"..DammazKronTNFO.TmpTargetInfo[s_targetName][1].."<-->"..DammazKronTNFO.TmpTargetInfo[s_targetName][2].."<-->"..tostring(DammazKronTNFO.CareerName(careerLineSex)).."<-")
		end
	end
	
	if ( targetClassification == "selfhostiletarget" ) then
	
		if ( DK_Config.GetSetting( "MiscHTScore" ) ~= 1 ) then return end
		
		local targetTable = DammazKron.data[_serverName][_characterName][s_targetName]
		
		if ( targetType == 5 and targetTable ~= nil ) then
			local score = ( targetTable[1] + targetTable[2] ) - ( targetTable[5] + targetTable[6] ) 
			local color = DammazKron.linkColor( score )
			LabelSetText( "DammazKronHTSLabel", L" "..DammazKron.GetNumSymbol(score) )
			LabelSetTextColor( "DammazKronHTSLabel", color.r, color.g, color.b) 
			WindowSetShowing( "DammazKronHTS", true)
			
		else
			LabelSetText("DammazKronHTSLabel", L"")
			WindowSetShowing( "DammazKronHTS", false)
		end
	end
end

----

function DammazKronTNFO.CareerName( v_careerLineSex )
	local careerLine = string.sub( v_careerLineSex, 1, -2 )
	local careerSex = string.sub( v_careerLineSex, -1 )

	if( careerLine == "" ) then return L"" end
	if( careerSex == "1" ) then careerSex = "CareerLinesFemale" else careerSex = "CareerLinesMale" end

	return GetStringFromTable( careerSex , tonumber( careerLine ) )
end

--[[
targetTypes{ 0 = No Target, 1 = Player, 2 = Player's Pet, 3 = Friendly PC, 4 = Friendly NPC/Pet, 5 = Hostile Player, 6 = Hostile NPC/Pet, 7 = static }
--]]