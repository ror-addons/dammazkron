--------------------------------------------------------------------------------
-- File:      Core/DK_Lang.lua
-- Date:      16:42 13/04/2009
-- Author:    Valkea
-- File Rev:  36
-- Copyright: 2009
-- 
-- English : Valkea, tortall, Pistoleer
-- French  : Valkea
-- German  : gutgut, darud, Zint, AmonCarroburg
-- Italian : niclasam
-- Russian : Aazh, abpongo
-- Spanish : Araldi
--------------------------------------------------------------------------------

local _languageTable = {"enUS", "frFR", "deDE", "itIT", "esES", "koKR", "zhCN", "zhTW", "jaJP", "ruRU"}
local _language = _languageTable[SystemData.Settings.Language.active]

DammazKron_lang = {}
DammazKron_lang[_language] = {}
local loc = DammazKron_lang[_language]

function DammazKron.GetLocalString(StringID, Params)
	local returnString = DammazKron_lang[_language][StringID]
	
	if(Params ~= nil) then
		for i,v in ipairs(Params) do returnString = wstring.gsub(returnString, L"{v"..i..L"}", towstring(Params[i]) ) end
	end

	return returnString
end

function DammazKron.GetLocalTable(TableID)
	return DammazKron_lang[_language][TableID]
end

------------------------------------
-- 1 - English ---------------------
------------------------------------

if(_language == "enUS") then

	-- Print strings
	
	loc.welcome = L"Dammaz Kron v"..DammazKron.version..L" initialized."
	loc.errorCLog = L"The Combat Log is inactive. In this state, Dammaz Kron can't register events but you can still access your registered data."

	loc.alertDeathBlow = L"You have finished off<br>{v1}"
	loc.alertAssist = L"You helped to kill<br>{v1}"
	loc.alertDeath = L"You have been finished off by<br>{v1}"
	loc.alertLine2 = L"{v1} ({v2} lvl {v3})"
	loc.alertLine3 = L"Score {v1}"

	loc.printAll = L"{v1} ({v2} lvl {v3}) - K({v4}-{v5}) V({v6}-{v7}) D({v8}-{v9}) Score:{v10}"
	loc.printRecount = L"Totals - K({v1}-{v2}) V({v3}-{v4}) D({v5}-{v6}) - Profiles : {v7}"
	
	loc.version = L"Dammaz Kron v"..DammazKron.version
	loc.promptError = L"\"/dk {v1}\" is not a valid Dammaz Kron command. Use \"/dk help\" to get the commands list."

	-- TOK page
	
	loc.DK_Title_Line1 = L"Dammaz Kron"
	loc.DK_Title_Line2 = L"The Great Book<br>of Grudges"

	loc.DK_HeaderTitleLeft = loc.DK_Title_Line1
	loc.DK_HeaderTitleRightTOK = L""

	loc.DK_TitleLifeTime = L"Life Time RvR stats"
	loc.DK_TitleSession = L"Session RvR stats"

	loc.DK_LabelDeathBlows = L"Deathblows :"
	loc.DK_LabelKills = L"Victories :"
	loc.DK_LabelDeaths = L"Defeats :"
	loc.DK_LabelScore = L"Score :"
	
	loc.DK_LastDeathBlow = L"Last deathblow :"
	loc.DK_LastKill = L"Last victory :"
	loc.DK_LastDeath = L"Last defeat :"
	loc.DK_LastSeen = L"Last interaction :"

	loc.DK_TimeFormat = L"{v2}-{v3}  {v4}:{v5} {v7}" -- v1 = year, v2 = month, v3 = day, v4 = hour, v5 = min, v6 = sec, v7 = am/pm
	loc.DK_TimeAMPM = true

	loc.DK_LeftMenu1 = L"Overall<br>Grudges"
	loc.DK_LeftMenu2 = L"Open Battle<br>Grudges"
	loc.DK_LeftMenu3 = L"Scenario<br>Grudges"
	loc.DK_LeftMenu4 = L"The last grudge"

	-- TOP pages

	loc.DK_HeaderTitleRightTOP1 = L"Overall Grudges"
	loc.DK_HeaderTitleRightTOP2 = L"Open Battle Grudges"
	loc.DK_HeaderTitleRightTOP3 = L"Scenario Grudges"

	loc.DK_OrderBy = L"Sort by {v1}"
	loc.DK_Header1 = L"Deathblow"
	loc.DK_Header2 = L"Victory"
	loc.DK_Header3 = L"Defeat"
	loc.DK_Header4 = L"Score"
	loc.DK_Header5 = L"Career"
	loc.DK_Header6 = L"Level"
	loc.DK_Header7 = L"Name"
	loc.DK_Header8 = L"Date"

	loc.DK_HeaderDesc1 = L"Deathblows are added when you finish off an opponent."
	loc.DK_HeaderDesc2 = L"Victories are added when you help to kill an opponent."
	loc.DK_HeaderDesc3 = L"Defeats are added when an opponent kills you."
	loc.DK_HeaderDesc4 = L"The score is the difference between deathblows and defeats."

	-- NFO pages

	loc.DK_HeaderTitleRightNFO = L"{v1}'s profile"

	loc.DK_NFO_Name = L"{v1}"
	loc.DK_NFO_Level = L"Level {v1}"
	loc.DK_NFO_CareerLevel = L"{v1} - Level {v2}"
	loc.DK_NFO_NoInfos = L"Unknown level and career"

	loc.DK_NFO_LifeTimeTitle = loc.DK_HeaderTitleRightTOP1
	loc.DK_NFO_OpenTitle = loc.DK_HeaderTitleRightTOP2
	loc.DK_NFO_ScenarioTitle = loc.DK_HeaderTitleRightTOP3

	loc.DK_NFO_MiniProfilTracker = L"Open a mini profile window"

	-- Config window

	loc.DKconfig_Title = loc.version
	loc.DKconfig_Title_DeathBlows = L"Deathblows Options"
	loc.DKconfig_Title_Assists = L"Shared victories Options"
	loc.DKconfig_Title_Deaths = L"Deaths Options"
	loc.DKconfig_Title_Misc = L"Miscellaneous Options"

	loc.DKconfig_Print = L"Chat-log alert message"
	loc.DKconfig_Alert = L"Middle-screen alert message"
	loc.DKconfig_Sound = L"Sound alert"

	loc.DKconfig_Hyperlink = L"Enable hyperlinks over names in chat-log messages"
	loc.DKconfig_HyperlinkSC = L"Score-colored names"

	loc.DKconfig_Moth = L"Enable Score display in \"Moth\"<br>(optional dependency)"

	loc.DKconfig_HTScore = L"Display the current hostile target score"
	
	-- Tables
	
	loc.searchAssist = {}
	loc.searchAssist[1] = L"(.%S+) has been .+ by (.%S+)['’]s .+"

	loc.usage = {}
	loc.usage[1] = L"Dammaz Kron commands:"
	loc.usage[2] = L"/dk config - toggle the configuration panel."
	loc.usage[3] = L"/dk version - return Dammaz Kron version number."
	loc.usage[4] = L"/dk debug - toggle the debug mode (Use ChatFilter \"Channel 7\", \"Channel 8\" and \"Channel 9\")."
	
	loc.usageDebug = {}
	loc.usageDebug[1] = L"____ specials commands ____"
	loc.usageDebug[2] = L"/dk count - recount deathblows, assists and defeats totals"
	loc.usageDebug[3] = L"/dk verifydb - Verify and modify is needed the structure of the Dammaz Kron Table."
	loc.usageDebug[4] = L"/dk checkall - Try to fill the incomplete profiles already in the Dammaz Kron Table, using all informations collected in the current session."
	loc.usageDebug[5] = L"/dk list - return datas registered for the current character."

	
------------------------------------
-- 2 - French ----------------------
------------------------------------

elseif(_language == "frFR") then
	
	-- Print strings
	
	loc.welcome = L"Dammaz Kron v"..DammazKron.version..L" initialisé."
	loc.errorCLog = L"L'affichage des messages de combat est désactivé. Dammaz Kron ne va donc rien enregistrer, mais vous pouvez accèder aux données déjà enregistrées."

	loc.alertDeathBlow = L"Vous avez achevé<br>{v1}"
	loc.alertAssist = L"Vous avez pris part à la mort de<br>{v1}"
	loc.alertDeath = L"Vous avez été tué par<br>{v1}"
	loc.alertLine2 = L"{v1} ({v2} lvl {v3})"
	loc.alertLine3 = L"Score {v1}"

	loc.printAll = L"{v1} ({v2} lvl {v3}) - K({v4}-{v5}) V({v6}-{v7}) D({v8}-{v9}) Score:{v10}"
	loc.printRecount = L"Totaux - K({v1}-{v2}) V({v3}-{v4}) D({v5}-{v6}) - Profils : {v7}"
	
	loc.version = L"Dammaz Kron v"..DammazKron.version
	loc.promptError = L"\"/dk {v1}\" n'est pas une commande Dammaz Kron valide. Tapez \"/dk help\" pour voir la liste des commandes."

	-- TOK page
	
	loc.DK_Title_Line1 = L"Dammaz Kron"
	loc.DK_Title_Line2 = L"Le Grand Livre<br>des Rancunes"

	loc.DK_HeaderTitleLeft = loc.DK_Title_Line1
	loc.DK_HeaderTitleRightTOK = L""

	loc.DK_TitleLifeTime = L"Informations RvR globales"
	loc.DK_TitleSession = L"Informations RvR de la session"

	loc.DK_LabelDeathBlows = L"Coups mortels :"
	loc.DK_LabelKills = L"Victoires :"
	loc.DK_LabelDeaths = L"Défaites :"
	loc.DK_LabelScore = L"Score :"
	
	loc.DK_LastDeathBlow = L"Dernier coup mortel :"
	loc.DK_LastKill = L"Dernière victoire :"
	loc.DK_LastDeath = L"Dernière défaite :"
	loc.DK_LastSeen = L"Dernière confrontation :"

	loc.DK_TimeFormat = L"{v3}-{v2}-{v1} {v4}:{v5}" -- v1 = year, v2 = month, v3 = day, v4 = hour, v5 = min, v6 = sec, v7 = am/pm
	loc.DK_TimeAMPM = false

	loc.DK_LeftMenu1 = L"Toutes<br>mes rancunes"
	loc.DK_LeftMenu2 = L"Mes rancunes<br>de guerre"
	loc.DK_LeftMenu3 = L"Mes rancunes<br>de scénario"
	loc.DK_LeftMenu4 = L"La dernière rancune"

	-- TOP pages

	loc.DK_HeaderTitleRightTOP1 = L"Toutes mes rancunes"
	loc.DK_HeaderTitleRightTOP2 = L"Mes rancunes de guerre"
	loc.DK_HeaderTitleRightTOP3 = L"Mes rancunes de scénario"

	loc.DK_OrderBy = L"Classer par {v1}"
	loc.DK_Header1 = L"Coups mortel"
	loc.DK_Header2 = L"Victoires"
	loc.DK_Header3 = L"Défaites"
	loc.DK_Header4 = L"Scores"
	loc.DK_Header5 = L"Carrières"
	loc.DK_Header6 = L"Niveaux"
	loc.DK_Header7 = L"Noms"
	loc.DK_Header8 = L"Dates"

	loc.DK_HeaderDesc1 = L"Un coup mortel est compté lorsque vous avez achevé un adversaire."
	loc.DK_HeaderDesc2 = L"Une victoire est compté lorsque vous avez aidé a tuer un adversaire."
	loc.DK_HeaderDesc3 = L"Une défaite est compté lorsque qu'un adversaire vous a tué."
	loc.DK_HeaderDesc4 = L"Le score est la diffèrence entre le nombre de coups mortels que vous avez porté et le nombre de défaites qui vous a été infligé."

	-- NFO pages

	loc.DK_HeaderTitleRightNFO = L"Profil de {v1}"

	loc.DK_NFO_Name = L"{v1}"
	loc.DK_NFO_Level = L"Niveau {v1}"
	loc.DK_NFO_CareerLevel = L"{v1} - Niveau {v2}"
	loc.DK_NFO_NoInfos = L"Carrière et niveau indéterminés"

	loc.DK_NFO_LifeTimeTitle = loc.DK_HeaderTitleRightTOP1
	loc.DK_NFO_OpenTitle = loc.DK_HeaderTitleRightTOP2
	loc.DK_NFO_ScenarioTitle = loc.DK_HeaderTitleRightTOP3

	loc.DK_NFO_MiniProfilTracker = L"Ouvrir le mini-profil"

	-- Config window

	loc.DKconfig_Title = loc.version
	loc.DKconfig_Title_DeathBlows = L"Coups mortels"
	loc.DKconfig_Title_Assists = L"Victoires partagées"
	loc.DKconfig_Title_Deaths = L"Défaites"
	loc.DKconfig_Title_Misc = L"Divers"

	loc.DKconfig_Print = L"Message d'alerte dans le chat"
	loc.DKconfig_Alert = L"Message d'alerte au centre de l'écran"
	loc.DKconfig_Sound = L"Alerte sonore"

	loc.DKconfig_Hyperlink = L"Activer les hyperliens sur les noms dans les messages du chat"
	loc.DKconfig_HyperlinkSC = L"Couleur des noms en fonction du score"

	loc.DKconfig_Moth = L"Afficher le score dans \"Moth\" (addon optionnel)"
	
	loc.DKconfig_HTScore = L"Afficher le score de la cible en cours"
	
	-- Tables
	
	loc.searchAssist = {}
	loc.searchAssist[1] = L"(.%S+) .+ par .+ d['’e]%s?(.%S+) dans .+"
	
	loc.usage = {}
	loc.usage[1] = L"Liste des commandes de Dammaz Kron :"
	loc.usage[2] = L"/dk config - ouvre le panneau de configuration."
	loc.usage[3] = L"/dk version - affiche la version."
	loc.usage[4] = L"/dk debug - active / désactive le mode de débugage ( Utilise les Filtres de Chat \"Cannal 7\", \"Cannal 8\" et \"Cannal 9\" )."
	
	loc.usageDebug = {}
	loc.usageDebug[1] = L"____ Commandes spéciales ____"
	loc.usageDebug[2] = L"/dk count - Re-compte l'ensemble des coups mortels, des victoires partagées et des défaites."
	loc.usageDebug[3] = L"/dk verifydb - Vérifie et corrige la structure de la table stockant les informations."
	loc.usageDebug[4] = L"/dk checkall - Tente de compléter les profils incomplets présent dans la DB de Dammaz Kron en utilisant les informations collectées durant cette session."
	loc.usageDebug[5] = L"/dk list - affiche l'ensemble des données enregistrées pour ce personnage."


------------------------------------
-- 3 - Deutch ----------------------
------------------------------------

elseif(_language == "deDE") then
	
	-- Print strings
	
   	loc.welcome = L"Dammaz Kron v"..DammazKron.version..L" initialisiert."
	loc.errorCLog = L"Das Combat Log ist nicht aktiv. In diesem Zustand ist Dammaz Kron nicht in der Lage neue Tode aufzuzeichnen, aber du kannst immernoch auf alte Daten zugreifen."

	loc.alertDeathBlow = L"Du hast<br>{v1} getötet"
	loc.alertAssist = L"Du hast mitgeholfen<br>{v1} zu töten"
	loc.alertDeath = L"Du wurdest von<br>{v1} getötet"
	loc.alertLine2 = L"{v1} ({v2} lvl {v3})"
	loc.alertLine3 = L"Punkte {v1}"

	loc.printAll = L"{v1} ({v2} lvl {v3}) - K({v4}-{v5}) V({v6}-{v7}) D({v8}-{v9}) Punkte:{v10}"
	loc.printRecount = L"Gesamt- K({v1}-{v2}) V({v3}-{v4}) D({v5}-{v6}) - Profils : {v7}"
	
	loc.version = L"Dammaz Kron v"..DammazKron.version
	loc.promptError = L"\"/dk {v1}\" ist kein gültiger Dammaz Kron Befehl. Bitte \"/dk help\" benutzen um eine Befehlsliste zu erhalten."

	-- TOK page
	
	loc.DK_Title_Line1 = L"Dammaz Kron"
	loc.DK_Title_Line2 = L"Das Buch des Grolls"

	loc.DK_HeaderTitleLeft = loc.DK_Title_Line1
	loc.DK_HeaderTitleRightTOK = L""

	loc.DK_TitleLifeTime = L"Gesamt RVR Statistik"
	loc.DK_TitleSession = L"Sitzung RVR Statistik"

	loc.DK_LabelDeathBlows = L"Todesstöße :"
	loc.DK_LabelKills = L"Siege :"
	loc.DK_LabelDeaths = L"Niederlagen :"
	loc.DK_LabelScore = L"Punkte :"
	
	loc.DK_LastDeathBlow = L"Letzter Todesstoß :"
	loc.DK_LastKill = L"Letzter Sieg :"
	loc.DK_LastDeath = L"Letzte Niederlage :"
	loc.DK_LastSeen = L"Letzte Interaktion :"

	loc.DK_TimeFormat = L"{v2}-{v3}  {v4}:{v5} {v7}" -- v1 = year, v2 = month, v3 = day, v4 = hour, v5 = min, v6 = sec, v7 = am/pm
	loc.DK_TimeAMPM = true

	loc.DK_LeftMenu1 = L"Gesamt<br>Statistik"
	loc.DK_LeftMenu2 = L"Offener Kampf<br>Statistik"
	loc.DK_LeftMenu3 = L"Scenario<br>Statistik"
	loc.DK_LeftMenu4 = L"Der letzte Tod"

	-- TOP pages

	loc.DK_HeaderTitleRightTOP1 = L"Gesamt - Statistik"
	loc.DK_HeaderTitleRightTOP2 = L"Offener - Kampf Statistik"
	loc.DK_HeaderTitleRightTOP3 = L"Scenario - Statistik"

	loc.DK_OrderBy = L"Nach {v1} sortieren"
	loc.DK_Header1 = L"Todesstößen"
	loc.DK_Header2 = L"Siegen"
	loc.DK_Header3 = L"Niederlagen"
	loc.DK_Header4 = L"Punkten"
	loc.DK_Header5 = L"Karriere"
	loc.DK_Header6 = L"Level"
	loc.DK_Header7 = L"Name"
	loc.DK_Header8 = L"Datum"

	loc.DK_HeaderDesc1 = L"Todesstöße werden gezählt, wenn du dem Feind den finalen Schlag versetzt."
	loc.DK_HeaderDesc2 = L"Siege werden gezählt, wenn du mithilfst einen Feind zu töten."
	loc.DK_HeaderDesc3 = L"Niederlagen werden gezählt, wenn du von einem Gegner umgebracht wirst."
	loc.DK_HeaderDesc4 = L"Der Punktestand ist die Differenz zwischen Todestößen und Niederlagen."

	-- NFO pages

	loc.DK_HeaderTitleRightNFO = L"Profil von {v1}"

	loc.DK_NFO_Name = L"{v1}"
	loc.DK_NFO_Level = L"Level {v1}"
	loc.DK_NFO_CareerLevel = L"{v1} - Level {v2}"
	loc.DK_NFO_NoInfos = L"Level und Karriere sind unbekannt"

	loc.DK_NFO_LifeTimeTitle = loc.DK_HeaderTitleRightTOP1
	loc.DK_NFO_OpenTitle = loc.DK_HeaderTitleRightTOP2
	loc.DK_NFO_ScenarioTitle = loc.DK_HeaderTitleRightTOP3

	loc.DK_NFO_MiniProfilTracker = L"Mini-Profil-Tracker öffnen"

	-- Config window

	loc.DKconfig_Title = loc.version
	loc.DKconfig_Title_DeathBlows = L"Todesstoß Optionen"
	loc.DKconfig_Title_Assists = L"Sieg Optionen"
	loc.DKconfig_Title_Deaths = L"Todes Optionen"
	loc.DKconfig_Title_Misc = L"Diverse Optionen"

	loc.DKconfig_Print = L"Chat Meldungen"
	loc.DKconfig_Alert = L"Bildschirm Meldungen"
	loc.DKconfig_Sound = L"Akustisches Signal"

	loc.DKconfig_Hyperlink = L"Links für Namen im Chat aktivieren"
	loc.DKconfig_HyperlinkSC = L"Nach Punktestand gefärbte Namen"
	
	loc.DKconfig_Moth = L"Aktiviert die Punktestandanzeige in \"Moth\" (optional dependency)"
	
	loc.DKconfig_HTScore = L"Aktiviert das Mini-Punktestandfenster auf dem Bildschirm"
	
	-- Tables
	
	loc.searchAssist = {}
	loc.searchAssist[1] = L"(.%S+) wurde von (.%S+) .+"

	loc.usage = {}
	loc.usage[1] = L"Dammaz Kron Befehle:"
	loc.usage[2] = L"/dk config - öffnet das Optionsmenü."
	loc.usage[3] = L"/dk version - gibt die Versionsnummer zurück."
	loc.usage[4] = L"/dk debug - schaltet den Debug-Modus ein (\"Channel 7\", \"Channel 8\" and \"Channel 9\")."
	
	loc.usageDebug = {}
	loc.usageDebug[1] = L"____ specials commands ____"
	loc.usageDebug[2] = L"/dk count - recount deathblows, assists and defeats totals"
	loc.usageDebug[3] = L"/dk verifydb - Verify and modify is needed the structure of the Dammaz Kron Table."
	loc.usageDebug[4] = L"/dk checkall - Try to fill the incomplete profiles already in the Dammaz Kron Table, using all informations collected in the current session."
	loc.usageDebug[5] = L"/dk list - return datas registered for the current character."

	
------------------------------------
-- 4 - Italian ---------------------
------------------------------------

elseif(_language == "itIT") then
	
	-- Print strings
	
	loc.welcome = L"Dammaz Kron v"..DammazKron.version..L" inizializzato."
	loc.errorCLog = L"I log di combattimento sono disattivati. In questo modo Dammaz Kron non può continuare a registrare gli eventi, ma puoi continuare a consultare quelli già registrati."

	loc.alertDeathBlow = L"Hai dato il colpo mortale a<br>{v1}"
	loc.alertAssist = L"Hai contribuito ad uccidere<br>{v1}"
	loc.alertDeath = L"Sei stato ucciso da<br>{v1}"
	loc.alertLine2 = L"{v1} ({v2} Livello {v3})"
	loc.alertLine3 = L"Punteggio {v1}"

	loc.printAll = L"{v1} ({v2} lvl {v3}) - K({v4}-{v5}) V({v6}-{v7}) D({v8}-{v9}) Punteggio:{v10}"
	loc.printRecount = L"Totals - K({v1}-{v2}) V({v3}-{v4}) D({v5}-{v6}) - Profils : {v7}"
	
	loc.version = L"Dammaz Kron v"..DammazKron.version
	loc.promptError = L"\"/dk {v1}\" non è un comando valido di Dammaz Kron. Usa \"/dk help\" per la lista dei comandi."

	-- TOK page
	
	loc.DK_Title_Line1 = L"Dammaz Kron"
	loc.DK_Title_Line2 = L"Il Grande Libro<br>della Vendetta"

	loc.DK_HeaderTitleLeft = loc.DK_Title_Line1
	loc.DK_HeaderTitleRightTOK = L""

	loc.DK_TitleLifeTime = L"Statistiche RvR"
	loc.DK_TitleSession = L"Statistiche Sessioni Rvr"

	loc.DK_LabelDeathBlows = L"Colpi mortali :"
	loc.DK_LabelKills = L"Vittorie :"
	loc.DK_LabelDeaths = L"Sconfitte :"
	loc.DK_LabelScore = L"Punteggio :"
	
	loc.DK_LastDeathBlow = L"Ultimo colpo mortale :"
	loc.DK_LastKill = L"Ultima vittoria :"
	loc.DK_LastDeath = L"Ultima sconfitta :"
	loc.DK_LastSeen = L"Ultima collaborazione :"

	loc.DK_TimeFormat = L"{v2}-{v3}  {v4}:{v5} {v7}" -- v1 = year, v2 = month, v3 = day, v4 = hour, v5 = min, v6 = sec, v7 = am/pm
	loc.DK_TimeAMPM = true

	loc.DK_LeftMenu1 = L"Riepilogo"
	loc.DK_LeftMenu2 = L"Battaglie<br>in campo aperto"
	loc.DK_LeftMenu3 = L"Scenari"
	loc.DK_LeftMenu4 = L"L'ultima vendetta"

	-- TOP pages

	loc.DK_HeaderTitleRightTOP1 = L"Riepilogo"
	loc.DK_HeaderTitleRightTOP2 = L"Battaglie in campo aperto"
	loc.DK_HeaderTitleRightTOP3 = L"Scenari"

	loc.DK_OrderBy = L"Ordina per {v1}"
	loc.DK_Header1 = L"Colpo mortale"
	loc.DK_Header2 = L"Vittoria"
	loc.DK_Header3 = L"Sconfitta"
	loc.DK_Header4 = L"Punteggio"
	loc.DK_Header5 = L"Carriera"
	loc.DK_Header6 = L"Livello"
	loc.DK_Header7 = L"Nome"
	loc.DK_Header8 = L"Data"

	loc.DK_HeaderDesc1 = L"Sono considerati colpi mortali quando finisci un nemico."
	loc.DK_HeaderDesc2 = L"Sono considerate vittorie quando contribuisci ad uccidere un nemico."
	loc.DK_HeaderDesc3 = L"Sono considerate sconfitte quando un nemcio ti uccide."
	loc.DK_HeaderDesc4 = L"Il punteggio è la differenza tra colpi mortali e sconfitte."

	-- NFO pages

	loc.DK_HeaderTitleRightNFO = L"Profilo di {v1}"

	loc.DK_NFO_Name = L"{v1}"
	loc.DK_NFO_Level = L"Level {v1}"
	loc.DK_NFO_CareerLevel = L"{v1} - Level {v2}"
	loc.DK_NFO_NoInfos = L"Livello e carriera sconosciuti"

	loc.DK_NFO_LifeTimeTitle = loc.DK_HeaderTitleRightTOP1
	loc.DK_NFO_OpenTitle = loc.DK_HeaderTitleRightTOP2
	loc.DK_NFO_ScenarioTitle = loc.DK_HeaderTitleRightTOP3

	loc.DK_NFO_MiniProfilTracker = L"Apri finestra del profilo"

	-- Config window

	loc.DKconfig_Title = loc.version
	loc.DKconfig_Title_DeathBlows = L"Opzioni finestra \"colpi mortali\""
	loc.DKconfig_Title_Assists = L"Opzioni finestra \"vittorie\""
	loc.DKconfig_Title_Deaths = L"Opzioni finestra \"morti\""
	loc.DKconfig_Title_Misc = L"Opzioni varie"

	loc.DKconfig_Print = L"Messaggi di allarme della chat-log"
	loc.DKconfig_Alert = L"Middle-screen alert message"
	loc.DKconfig_Sound = L"Allarme sonoro"

	loc.DKconfig_Hyperlink = L"Abilita link dei nomei nelle chat"
	loc.DKconfig_HyperlinkSC = L"Nomi colorati in base ai punteggi"
	
	loc.DKconfig_Moth = L"Enable Score display in \"Moth\"<br>(optional dependency)"
	
	loc.DKconfig_HTScore = L"Display the current hostile target score"
	
	-- Tables
	
	loc.searchAssist = {}
	loc.searchAssist[1] = L"(.%S+) è .+ d['’i]%s?(.%S+) a .+"

	loc.usage = {}
	loc.usage[1] = L"Dammaz Kron commands:"
	loc.usage[2] = L"/dk config - apre il pannello di configurazione."
	loc.usage[3] = L"/dk version - mostra la versione corrente di DK."
	loc.usage[4] = L"/dk debug - pannello debug (ChatFilter \"Channel 7\", \"Channel 8\" and \"Channel 9\")."
	
	loc.usageDebug = {}
	loc.usageDebug[1] = L"____ specials commands ____"
	loc.usageDebug[2] = L"/dk count - recount deathblows, assists and defeats totals"
	loc.usageDebug[3] = L"/dk verifydb - Verify and modify is needed the structure of the Dammaz Kron Table."
	loc.usageDebug[4] = L"/dk checkall - Try to fill the incomplete profiles already in the Dammaz Kron Table, using all informations collected in the current session."
	loc.usageDebug[5] = L"/dk list - return datas registered for the current character."
	
	
------------------------------------
-- 5 - Spanish ---------------------
------------------------------------

elseif(_language == "esES") then

	-- Print strings
	
	loc.welcome = L"Dammaz Kron v"..DammazKron.version..L" inicializado."
	loc.errorCLog = L"El registro de combate está desactivado. En este estado, Dammaz Kron no puede registrar sucesos pero todavía puedes acceder a tus datos grabados."

	loc.alertDeathBlow = L"Has liquidado a<br>{v1}"
	loc.alertAssist = L"Has ayudado a matar a<br>{v1}"
	loc.alertDeath = L"Te ha liquidado<br>{v1}"
	loc.alertLine2 = L"{v1} ({v2} lvl {v3})"
	loc.alertLine3 = L"Score {v1}"

	loc.printAll = L"{v1} ({v2} lvl {v3}) - K({v4}-{v5}) V({v6}-{v7}) D({v8}-{v9}) Score:{v10}"
	loc.printRecount = L"Total - K({v1}-{v2}) V({v3}-{v4}) D({v5}-{v6}) - Perfiles : {v7}"
	
	loc.version = L"Dammaz Kron v"..DammazKron.version
	loc.promptError = L"\"/dk {v1}\" no es un comando válido de Dammaz Kron. Usa \"/dk help\" para acceder a la lista de comandos."

	-- TOK page
	
	loc.DK_Title_Line1 = L"Dammaz Kron"
	loc.DK_Title_Line2 = L"El Gran Libro<br>de las Rencillas"

	loc.DK_HeaderTitleLeft = loc.DK_Title_Line1
	loc.DK_HeaderTitleRightTOK = L""

	loc.DK_TitleLifeTime = L"Estadísticas RvR históricas"
	loc.DK_TitleSession = L"Estadísticas RvR sesión"

	loc.DK_LabelDeathBlows = L"Golpes mortales :"
	loc.DK_LabelKills = L"Victorias :"
	loc.DK_LabelDeaths = L"Derrotas :"
	loc.DK_LabelScore = L"Puntuación :"
	
	loc.DK_LastDeathBlow = L"Ultimo golpe mortal :"
	loc.DK_LastKill = L"Ultima victoria :"
	loc.DK_LastDeath = L"Ultima derrota :"
	loc.DK_LastSeen = L"Ultima interacción :"

	loc.DK_TimeFormat = L"{v2}-{v3}  {v4}:{v5} {v7}" -- v1 = year, v2 = month, v3 = day, v4 = hour, v5 = min, v6 = sec, v7 = am/pm
	loc.DK_TimeAMPM = true

	loc.DK_LeftMenu1 = L"Rencillas<br>generales"
	loc.DK_LeftMenu2 = L"Rencillas combate<br>a campo abierto"
	loc.DK_LeftMenu3 = L"Rencillas<br>de escenario"
	loc.DK_LeftMenu4 = L"La última rencilla"

	-- TOP pages

	loc.DK_HeaderTitleRightTOP1 = L"Rencillas generales"
	loc.DK_HeaderTitleRightTOP2 = L"Rencillas combate a campo abierto"
	loc.DK_HeaderTitleRightTOP3 = L"Rencillas de escenario"

	loc.DK_OrderBy = L"Ordenar por {v1}"
	loc.DK_Header1 = L"Golpe mortal"
	loc.DK_Header2 = L"Victoria"
	loc.DK_Header3 = L"Derrota"
	loc.DK_Header4 = L"Puntuación"
	loc.DK_Header5 = L"Carrera"
	loc.DK_Header6 = L"Nivel"
	loc.DK_Header7 = L"Nombre"
	loc.DK_Header8 = L"Fecha"

	loc.DK_HeaderDesc1 = L"Los golpes mortales se añaden cuando das el golpe de gracia a un adversario."
	loc.DK_HeaderDesc2 = L"Las victorias se añaden cuando ayudas a matar a un adversario."
	loc.DK_HeaderDesc3 = L"Las derrotas se añaden cuando un adversario te mata."
	loc.DK_HeaderDesc4 = L"La puntuación es la diferencia entre golpes mortales y derrotas."

	-- NFO pages

	loc.DK_HeaderTitleRightNFO = L"Perfil de {v1}"

	loc.DK_NFO_Name = L"{v1}"
	loc.DK_NFO_Level = L"Nivel {v1}"
	loc.DK_NFO_CareerLevel = L"{v1} - Nivel {v2}"
	loc.DK_NFO_NoInfos = L"Nivel y carrera desconocidos"

	loc.DK_NFO_LifeTimeTitle = loc.DK_HeaderTitleRightTOP1
	loc.DK_NFO_OpenTitle = loc.DK_HeaderTitleRightTOP2
	loc.DK_NFO_ScenarioTitle = loc.DK_HeaderTitleRightTOP3

	loc.DK_NFO_MiniProfilTracker = L"Abrir una ventana de miniperfil"

	-- Config window

	loc.DKconfig_Title = loc.version
	loc.DKconfig_Title_DeathBlows = L"Opciones de golpes mortales"
	loc.DKconfig_Title_Assists = L"Opciones de victorias compartidas"
	loc.DKconfig_Title_Deaths = L"Opciones de muertes"
	loc.DKconfig_Title_Misc = L"Opciones varias"

	loc.DKconfig_Print = L"Mensaje de alerta de chat"
	loc.DKconfig_Alert = L"Mensaje de alerta mitad de pantalla"
	loc.DKconfig_Sound = L"Alerta de sonido"

	loc.DKconfig_Hyperlink = L"Habilitar hipervínculos sobre nombres en el chat"
	loc.DKconfig_HyperlinkSC = L"Nombres coloreados según puntuación"
	
	loc.DKconfig_Moth = L"Activar mostrar puntuación en \"Moth\"<br>(optional dependency)"
	
	loc.DKconfig_HTScore = L"Mostrar la puntuación del actual objetivo enemigo"
	
	-- Tables
	
	loc.searchAssist = {}
	loc.searchAssist[1] = L"(.%S+) .+ por (.%S+)['’]s .+ en .+"

	loc.usage = {}
	loc.usage[1] = L"Comandos de Dammaz Kron:"
	loc.usage[2] = L"/dk config - Abrir/cerrar panel de configuración."
	loc.usage[3] = L"/dk version - Número de versión de Dammaz Kron"
	loc.usage[4] = L"/dk debug - Abrir/cerrar el modo debug (Use ChatFilter \"Channel 7\", \"Channel 8\" and \"Channel 9\")."
	
	loc.usageDebug = {}
	loc.usageDebug[1] = L"____ specials commands ____"
	loc.usageDebug[2] = L"/dk count - recount deathblows, assists and defeats totals"
	loc.usageDebug[3] = L"/dk verifydb - Verify and modify is needed the structure of the Dammaz Kron Table."
	loc.usageDebug[4] = L"/dk checkall - Try to fill the incomplete profiles already in the Dammaz Kron Table, using all informations collected in the current session."
	loc.usageDebug[5] = L"/dk list - return datas registered for the current character."

	
------------------------------------
-- 7 - Korean ----------------------
------------------------------------

elseif(_language == "koKR") then
	
	
------------------------------------
-- 8 - S_Chinese -------------------
------------------------------------

elseif(_language == "zhCN") then
	
	
------------------------------------
-- 9 - T_Chinese -------------------
------------------------------------

elseif(_language == "zhTW") then
	
	
------------------------------------
-- 10 - Japanese -------------------
------------------------------------

elseif(_language == "jaJP") then

	
------------------------------------
-- 11 - Russian --------------------
------------------------------------

elseif(_language == "ruRU") then
	-- Print strings
	
	loc.welcome = L"Dammaz Kron v"..DammazKron.version..L" инициализируется."
	loc.errorCLog = L"Отключён Combat Log. В таком режиме Dammaz Kron не может определять происходящие события, однако Вам по-прежнему доступны ранее записанные данные."

	loc.alertDeathBlow = L"Вы убили<br>{v1}"
	loc.alertAssist = L"Вы помогли убить<br>{v1}"
	loc.alertDeath = L"Вы убиты<br>{v1}"
	loc.alertLine2 = L"{v1} ({v2} Уровень {v3})"
	loc.alertLine3 = L"Результат {v1}"

	loc.printAll = L"{v1} ({v2} Уровень {v3}) - Вы убили({v4}-{v5}) Вы помогли убить({v6}-{v7}) Вы убиты({v8}-{v9}) Результат:{v10}"
	loc.printRecount = L"Итоги - Вы убили({v1}-{v2}) Вы помогли убить({v3}-{v4}) Вы убиты{v5}-{v6}) - Профили : {v7}"
	
	loc.version = L"Dammaz Kron v"..DammazKron.version
	loc.promptError = L"\"/dk {v1}\" is not a valid Dammaz Kron command. Use \"/dk help\" to get the commands list."

	-- TOK page
	
	loc.DK_Title_Line1 = L"Dammaz Kron"
	loc.DK_Title_Line2 = L"Великая Книга<br>Недовольство"

	loc.DK_HeaderTitleLeft = loc.DK_Title_Line1
	loc.DK_HeaderTitleRightTOK = L""

	loc.DK_TitleLifeTime = L"Общая статистика РвР"
	loc.DK_TitleSession = L"РвР статистика сессии"

	loc.DK_LabelDeathBlows = L"Смертельных ударов:"
	loc.DK_LabelKills = L"Побед:"
	loc.DK_LabelDeaths = L"Поражений:"
	loc.DK_LabelScore = L"Результат:"
	
	loc.DK_LastDeathBlow = L"Последний Смертельный Удар:"
	loc.DK_LastKill = L"Последняя победа:"
	loc.DK_LastDeath = L"Последнее поражение:"
	loc.DK_LastSeen = L"Последняя битва:"

	loc.DK_TimeFormat = L"{v2}-{v3}  {v4}:{v5} {v7}" -- v1 = year, v2 = month, v3 = day, v4 = hour, v5 = min, v6 = sec, v7 = am/pm
	loc.DK_TimeAMPM = true

	loc.DK_LeftMenu1 = L"Суммарная<br>статистика"
	loc.DK_LeftMenu2 = L"Открытые<br>сражения"
	loc.DK_LeftMenu3 = L"Сценарий"
	loc.DK_LeftMenu4 = L"Последний профиль"

	-- TOP pages

	loc.DK_HeaderTitleRightTOP1 = L"Суммарная статистика"
	loc.DK_HeaderTitleRightTOP2 = L"Открытые сражения"
	loc.DK_HeaderTitleRightTOP3 = L"Сценарий"

	loc.DK_OrderBy = L"Сортировать по {v1}"
	loc.DK_Header1 = L"Смертельный удар"
	loc.DK_Header2 = L"Победа"
	loc.DK_Header3 = L"Поражение"
	loc.DK_Header4 = L"Результат"
	loc.DK_Header5 = L"Карьера"
	loc.DK_Header6 = L"Уровень"
	loc.DK_Header7 = L"Имя"
	loc.DK_Header8 = L"Время"

	loc.DK_HeaderDesc1 = L"Смертельный удар добавляется, когда вы убиваете противника."
	loc.DK_HeaderDesc2 = L"Победа добавляется, если вы помогли кому-то убить противника."
	loc.DK_HeaderDesc3 = L"Поражение добавляется, когда вас убивает соперникu."
	loc.DK_HeaderDesc4 = L"Результат, это соотношение между Смертельным ударом и Поражением."

	-- NFO pages

	loc.DK_HeaderTitleRightNFO = L"{v1} профиль"

	loc.DK_NFO_Name = L"{v1}"
	loc.DK_NFO_Level = L"Уровень {v1}"
	loc.DK_NFO_CareerLevel = L"{v1} - Уровень {v2}"
	loc.DK_NFO_NoInfos = L"Неизвестный уровень и карьера"

	loc.DK_NFO_LifeTimeTitle = loc.DK_HeaderTitleRightTOP1
	loc.DK_NFO_OpenTitle = loc.DK_HeaderTitleRightTOP2
	loc.DK_NFO_ScenarioTitle = loc.DK_HeaderTitleRightTOP3

	loc.DK_NFO_MiniProfilTracker = L"Open a mini profile window"

	-- Config window

	loc.DKconfig_Title = loc.version
	loc.DKconfig_Title_DeathBlows = L"Настройки Смертельного удара"
	loc.DKconfig_Title_Assists = L"Настройки Совместных побед"
	loc.DKconfig_Title_Deaths = L"Настройки Поражений"
	loc.DKconfig_Title_Misc = L"Прочие настройки"

	loc.DKconfig_Print = L"Включить сообщения в окне чата"
	loc.DKconfig_Alert = L"Включение сообщение в центре экрана"
	loc.DKconfig_Sound = L"Включить звук"

	loc.DKconfig_Hyperlink = L"Включить гиперлинк (горячее нажатие) на имени игрока в окне чата"
	loc.DKconfig_HyperlinkSC = L"Цветовая градация имен"

	loc.DKconfig_Moth = L"Enable Score display in \"Moth\"<br>(optional dependency)"
	
	loc.DKconfig_HTScore = L"Display the current hostile target score"
	
	-- Tables
	
	loc.searchAssist = {}
	loc.searchAssist[1] = L"Игрок (.%S+) .+ игроком (.%S+) .+"
	loc.searchAssist[2] = L"(.%S+) has been .+ by (.%S+)['’]s .+"

	loc.usage = {}
	loc.usage[1] = L"Команды Dammaz Kron:"
	loc.usage[2] = L"/dk config - меню настроек."
	loc.usage[3] = L"/dk version - номер версии Dammaz Kron."
	loc.usage[4] = L"/dk debug - включить режим отладки (ChatFilter \"Channel 7\", \"Channel 8\" and \"Channel 9\")."
	
	loc.usageDebug = {}
	loc.usageDebug[1] = L"____ specials commands ____"
	loc.usageDebug[2] = L"/dk recount - recount deathblows, assists and defeats totals"
	loc.usageDebug[3] = L"/dk repairdb - Verify and modify is needed the structure of the Dammaz Kron Table."
	loc.usageDebug[4] = L"/dk checkall - Try to fill the incomplete profiles already in the Dammaz Kron Table, using all informations collected in the current session."
	loc.usageDebug[5] = L"/dk list - return datas registered for the current character."
end