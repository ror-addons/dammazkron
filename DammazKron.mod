<?xml version="1.0" encoding="UTF-8"?>
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
    <UiMod name="DammazKron" version="0.6.4" date="01/01/2009" >
        
        <Author name="Valkea" email="naaATfreeDOTfr" />
        <Description text="Dammaz Kron, The Great Book of Grudges ! Intends to record all your PVP deaths, assisted kills and deathblows in order to help you in revenging your grudges ;) Use /dk to call the menu." />
        
        <Dependencies>
		<Dependency name="EA_TomeOfKnowledge"/>
        </Dependencies>
        
        <Files>
		<File name="Core/DK_Core.xml"/>
		
		<File name="Core/DK_Core.lua"/>
		<File name="Core/DK_Utils.lua"/>
		<File name="Core/DK_TargetInfo.lua"/>
		<File name="Core/DK_Lang.lua"/>
		<File name="Core/DK_Plugins.lua"/>
            
		<File name="Core/Tome/DK_Tome.lua"/>
		<File name="Core/Tome/DK_TOK.xml"/>
		<File name="Core/Tome/DK_TOP.xml"/>
		<File name="Core/Tome/DK_NFO.xml"/>
            
		<File name="Core/ToolTip/DK_Tooltip.lua"/>
		<File name="Core/ToolTip/DK_Tooltip.xml"/>
            
		<File name="Conf/DK_Config.xml"/>
		<File name="Conf/DK_Config.lua"/>
            
		<File name="Fix/SetBookmarkFix.lua"/>
		<File name="Fix/TargetInfoFix.lua"/>
        </Files>
        
        <OnInitialize>
		<CallFunction name="DammazKron.Start" />
		<CallFunction name="SetBookmarkHook.Start" />
        </OnInitialize>
        
        <OnShutdown>
		<CallFunction name="DammazKron.Stop" />
        </OnShutdown>
        
        <SavedVariables>
		<SavedVariable name="DammazKron.data" />
        </SavedVariables>
        
    </UiMod>
</ModuleFile>